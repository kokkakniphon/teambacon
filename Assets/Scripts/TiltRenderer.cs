﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class TiltRenderer : MonoBehaviour
{

    [SerializeField, BoxGroup("Tilt Settings")]
    private float localTiltAngle = 53.5f;
    public static float tiltAngle = 53.5f;
    
    private void OnDrawGizmosSelected() 
    {
        this.transform.eulerAngles = new Vector3(tiltAngle, this.transform.localRotation.y, this.transform.localRotation.z);

    }

    [Button ("Apply Tilt Angle")]
    public void ApplyTiltAngle()
    {
        TiltRenderer.tiltAngle = localTiltAngle;
    }
}
