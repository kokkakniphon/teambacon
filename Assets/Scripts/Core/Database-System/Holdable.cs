﻿using UnityEngine;
using Luminosity.IO;

public enum HoldableType
{
    item,
    tool,
    seedBag,
    crop
}

[RequireComponent(typeof(Rigidbody))]
public class Holdable : MonoBehaviour
{
    public bool isHolding {get; private set;}
    public HoldableType holdableType {get; private set;}
    public PlayerID lastPlayerID;

    private Rigidbody rid;

    public delegate void NoArgs();
    public NoArgs onPickUp;
    public NoArgs onPutDown;

    public void PickUp(Transform targetTransform, PlayerID playerID)
    {
        isHolding = true;
        this.transform.parent = targetTransform;
        this.transform.localPosition = Vector3.zero;
        lastPlayerID = playerID;

        rid.isKinematic = true;

        if(onPickUp != null)
            onPickUp.Invoke();
    }

    public void PutDown()
    {
        isHolding = false;
        this.transform.parent = null;

        rid.isKinematic = false;

        if(onPutDown != null)
            onPutDown.Invoke();
    }

    public void Throw(Vector3 value_dir, float value_force)
    {
        PutDown();
        
        rid.AddForce(value_dir.normalized * value_force);
    }

    public void Throw(Vector3 value_dir)
    {
        PutDown();
        
        rid.AddForce(value_dir);
    }

    public void SetHoldableType(HoldableType value_type)
    {
        rid = this.GetComponent<Rigidbody>();
        holdableType = value_type;
    }

    public bool CanPickUp()
    {
        if(this.holdableType == HoldableType.item)
        {
            if(GetComponent<Items>().isInProcess == true)
                return false;
            else
                return true;
        }
        else
        {
            return true;
        }   
    }

    private void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.CompareTag("FallPlain"))
        {
            if(this.holdableType != HoldableType.tool)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
