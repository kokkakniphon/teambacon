﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;

public class RandomEventManager : MonoBehaviour
{
    [SerializeField, BoxGroup("RandomEvent Info")]
    public float durationDuringEvent;
    [SerializeField, BoxGroup("RandomEvent Info"), ReadOnly]
    public float durationCurrent;

    [SerializeField, BoxGroup("RandomEvent Info")]
    public GameObject Thunderprefab;

    [SerializeField, BoxGroup("RandomEvent Info")]
    public int thunderPerSec = 6;
    private int thunderPerSecCount;

    [SerializeField, BoxGroup("RandomEvent Info"), ReorderableList]
    List<Vector2> randomEventSpawnTimeRange = new List<Vector2>();

    [SerializeField, BoxGroup("Sound Info")]
    private AudioData sfxThunder;
    [SerializeField, BoxGroup("RandomEvent Info")]
    private Animator animOverlay;

    private bool isCheckPlayerThisRound = true;

    public static RandomEventManager instance;

    private void Awake() 
    {
        if(instance != null)
        {Destroy(instance);}
        instance = this;
    }

    void Start()
    {
        LevelManager.instance.onLevelStart += StartBeforeEventTimer;
        LevelManager.instance.onLevelEnd += StopEventTimer; 

        thunderPerSecCount = thunderPerSec;
    }

    private void OnDestroy() 
    {
        LevelManager.instance.onLevelStart -= StartBeforeEventTimer;
        LevelManager.instance.onLevelEnd -= StopEventTimer; 
    }

    private void StartBeforeEventTimer()
    {
        for (int i = 0; i < randomEventSpawnTimeRange.Count; i++)
        {
            StartCoroutine(SetEventStartTimer(Random.Range(randomEventSpawnTimeRange[i].x, randomEventSpawnTimeRange[i].y)));
        }
    }

    private void StartDuringEventTimer()
    {
        durationCurrent = durationDuringEvent;
        StartCoroutine(DuringEventTimer());
    }

    private void StopEventTimer()
    {
        StopAllCoroutines();
    }

    IEnumerator SetEventStartTimer(float time)
    {
        yield return new WaitForSeconds(time);
        animOverlay.SetTrigger("StartEvent");
        StartDuringEventTimer();
    }

    IEnumerator DuringEventTimer()
    {
        while(durationCurrent > 0)
        {
            durationCurrent -= 1.0f;

            RunEvent();
                
            yield return new WaitForSeconds(1.0f);
            AudioManager.instance.Play(sfxThunder);
            thunderPerSecCount = thunderPerSec;
        }

        animOverlay.SetTrigger("EndEvent");
    }
    
    void SpawnRandomThunder()
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        float randomX = Random.Range(meshRenderer.bounds.min.x, meshRenderer.bounds.max.x);
        float randomZ = Random.Range(meshRenderer.bounds.min.z, meshRenderer.bounds.max.z);

        RaycastHit hit;
        if(Physics.Raycast(new Vector3(randomX, meshRenderer.bounds.max.y + 4.0f, randomZ), Vector3.down, out hit))
        {
            Vector3 newPos = hit.point;
            newPos.y = 4f;
            Instantiate(Thunderprefab, newPos, Quaternion.identity);
        }
    }

    private void SpawnTargetThunder(PlayerID playerID)
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        PlayerController playerController = PlayerManager.instance.GetPlayerControllerByID(playerID);
        float coX = playerController.transform.position.x;
        float coZ = playerController.transform.position.z;

        RaycastHit hit;
        if(Physics.Raycast(new Vector3(coX, meshRenderer.bounds.max.y + 4.0f, coZ), Vector3.down, out hit))
        {
            Vector3 newPos = hit.point;
            newPos.y = 4f;
            Instantiate(Thunderprefab, newPos, Quaternion.identity);
        }
    }

    private void RunEvent()
    {
        if(isCheckPlayerThisRound == true)
        {
            List<PlayerController> allPlayerController = PlayerManager.instance.allPlayerController;

            for(int i = 0; i < allPlayerController.Count; i++)
            {
                float chance = KarmaManager.instance.GetEventModifier(allPlayerController[i].playerStat);
                if(CheckIfStrikeOnPlayer(chance) == true)
                {
                    SpawnTargetThunder(allPlayerController[i].playerStat.playerID);
                    thunderPerSec--;
                }
            }

            isCheckPlayerThisRound = false;
        }
        else
        {
            isCheckPlayerThisRound = true;
        }
        
        for(int i = 0; i < thunderPerSecCount; i++)
                SpawnRandomThunder();
    }

    private bool CheckIfStrikeOnPlayer(float chance)
    {
        int randomNum = Random.Range(1, 101);

        if(randomNum <= Mathf.RoundToInt(chance * 100))
        {
            return true;
        }
        else
        {   
            return false;
        }        
    }
}    

