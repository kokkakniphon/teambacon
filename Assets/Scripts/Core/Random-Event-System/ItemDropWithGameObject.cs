﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;

public class ItemDropWithGameObject : MonoBehaviour
{
    [ReorderableList]
    public GameObject[] itemList;
    private bool [] itemListAvailable;

    [ReorderableList]
    public GameObject[] spawnPosition;
    private bool[] spawnPositionAvailable;

    [SerializeField, BoxGroup("ItemDropEvent Info")]
    public float durationDuringEvent;
    [SerializeField, BoxGroup("ItemDropEvent Info"), ReadOnly]
    public float durationCurrent;

    [SerializeField, BoxGroup("ItemDropEvent Info")]
    public int itemPerSec = 6;
    private int itemPerSecCount;

    [SerializeField, BoxGroup("ItemDropEvent Info"), ReorderableList]
    List<Vector2> randomEventSpawnTimeRange = new List<Vector2>();

    [SerializeField, BoxGroup("Sound Info")]
    private AudioData sfxItemDrop;
    public static ItemDropWithGameObject instance;

    private void Awake() 
    {
        if(instance != null)
        {Destroy(instance);}
        instance = this;
    }

    void Start()
    {
        LevelManager.instance.onLevelStart += StartBeforeEventTimer;
        LevelManager.instance.onLevelEnd += StopEventTimer; 

        spawnPositionAvailable = new bool[spawnPosition.Length];
        for(int i = 0; i < spawnPositionAvailable.Length; i++)
        {
            spawnPositionAvailable[i] = true;
        }

        itemListAvailable = new bool[itemList.Length];
        for(int i = 0; i < itemListAvailable.Length; i++)
        {
            itemListAvailable[i] = true;
        }
    }

    private void OnDestroy() 
    {
        LevelManager.instance.onLevelStart -= StartBeforeEventTimer;
        LevelManager.instance.onLevelEnd -= StopEventTimer; 
    }

    private void StartBeforeEventTimer()
    {
        for (int i = 0; i < randomEventSpawnTimeRange.Count; i++)
        {
            StartCoroutine(SetEventStartTimer(Random.Range(randomEventSpawnTimeRange[i].x, randomEventSpawnTimeRange[i].y)));
        }
    }

    private void StartDuringEventTimer()
    {
        durationCurrent = durationDuringEvent;
        StartCoroutine(DuringEventTimer());
    }

    private void StopEventTimer()
    {
        StopAllCoroutines();
    }

    IEnumerator SetEventStartTimer(float time)
    {
        yield return new WaitForSeconds(time);
        
        StartDuringEventTimer();
    }

    IEnumerator DuringEventTimer()
    {
        while(durationCurrent > 0)
        {
            RunEvent();

            durationCurrent -= 1.0f;

            yield return new WaitForSeconds(1.0f);
            if(sfxItemDrop != null)
                AudioManager.instance.Play(sfxItemDrop);
            itemPerSecCount = itemPerSec;

        }

        ClearSpawnPointCheck();
    }
    
    void SpawnRandomItem()
    {
        Instantiate(RandomItem(), RandomSpawnPoint(), Quaternion.identity);   
    }

    Vector3 RandomSpawnPoint()
    {
        int randomPos = Random.Range(0, spawnPosition.Length);   
        GameObject gameobject = spawnPosition[randomPos];
        while(spawnPositionAvailable[randomPos] == false && itemPerSec * durationDuringEvent < spawnPositionAvailable.Length)
        {
            randomPos = Random.Range(0, spawnPosition.Length);
            gameobject = spawnPosition[randomPos];
        }

        spawnPositionAvailable[randomPos] = false;
        return gameobject.transform.position;
    }

    private void RunEvent()
    {
        for(int i = 0; i < itemPerSecCount; i++)
            SpawnRandomItem();
    }

    private GameObject RandomItem()
    {
        int randomPos = Random.Range(0, itemList.Length);
        while(itemListAvailable[randomPos] == false && CheckItemRotation() == true)
        {
            randomPos = Random.Range(0, itemList.Length);
        }

        itemListAvailable[randomPos] = false;
        return itemList[(randomPos)];
    }

    private void ClearSpawnPointCheck()
    {
        for(int i=0; i < spawnPositionAvailable.Length; i++)
        {
            spawnPositionAvailable[i] = true;
        }
    }

    private void ClearItemRotationCheck()
    {
        for(int i=0; i < itemListAvailable.Length; i++)
        {
            itemListAvailable[i] = true;
        }
    }

    private bool CheckItemRotation()
    {
        int count = 0;
        for(int i = 0; i < itemListAvailable.Length; i++)
        {
            if(itemListAvailable[i] == false)
                count++;
        }

        if(count == itemListAvailable.Length)
        {
            Debug.Log("Clear");
            ClearItemRotationCheck();
            return false;
        }
        else
        {
            return true;
        }
    }
}
