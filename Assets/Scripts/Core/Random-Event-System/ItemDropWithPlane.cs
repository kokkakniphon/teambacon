﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;

public class ItemDropWithPlane : MonoBehaviour
{
    [ReorderableList]
    public GameObject[] itemList;

    [SerializeField, BoxGroup("ItemDropEvent Info")]
    public float durationDuringEvent;
    [SerializeField, BoxGroup("ItemDropEvent Info"), ReadOnly]
    public float durationCurrent;

    [SerializeField, BoxGroup("ItemDropEvent Info")]
    public int itemPerSec = 6;
    private int itemPerSecCount;

    [SerializeField, BoxGroup("ItemDropEvent Info"), ReorderableList]
    List<Vector2> randomEventSpawnTimeRange = new List<Vector2>();

    [SerializeField, BoxGroup("Sound Info")]
    private AudioData sfxItemDrop;

    [SerializeField, BoxGroup("ItemDropEvent Info")]
    private int constructionLayer = 10;
    public static ItemDropWithPlane instance;
    private bool isCheckPlayerThisRound;

    private void Awake() 
    {
        if(instance != null)
        {Destroy(instance);}
        instance = this;
    }

    void Start()
    {
        LevelManager.instance.onLevelStart += StartBeforeEventTimer;
        LevelManager.instance.onLevelEnd += StopEventTimer; 

        itemPerSecCount = itemPerSec;
    }

    private void OnDestroy() 
    {
        LevelManager.instance.onLevelStart -= StartBeforeEventTimer;
        LevelManager.instance.onLevelEnd -= StopEventTimer; 
    }

    private void StartBeforeEventTimer()
    {
        for (int i = 0; i < randomEventSpawnTimeRange.Count; i++)
        {
            StartCoroutine(SetEventStartTimer(Random.Range(randomEventSpawnTimeRange[i].x, randomEventSpawnTimeRange[i].y)));
        }
    }

    private void StartDuringEventTimer()
    {
        durationCurrent = durationDuringEvent;
        StartCoroutine(DuringEventTimer());
    }

    private void StopEventTimer()
    {
        StopAllCoroutines();
    }

    IEnumerator SetEventStartTimer(float time)
    {
        yield return new WaitForSeconds(time);
        
        StartDuringEventTimer();
    }

    IEnumerator DuringEventTimer()
    {
        while(durationCurrent > 0)
        {
            durationCurrent -= 1.0f;

            RunEvent();
                
            yield return new WaitForSeconds(1.0f);
            if(sfxItemDrop != null)
                AudioManager.instance.Play(sfxItemDrop);
            itemPerSecCount = itemPerSec;
        }
    }
    
    void SpawnRandomThunder()
    {
        Vector3 randomLocation = RandomSpawnPoint();

        RaycastHit hit;
        if(Physics.Raycast(randomLocation, Vector3.down, out hit))
        {
            while(hit.transform.gameObject.layer == constructionLayer)
            {
                RandomSpawnPoint();
                Physics.Raycast(randomLocation, Vector3.down, out hit);
            }

            Vector3 newPos = hit.point;
            newPos.y = 11f;
            Instantiate(RandomItem(), newPos, Quaternion.identity);
        }
    }

    Vector3 RandomSpawnPoint()
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        float randomX = Random.Range(meshRenderer.bounds.min.x, meshRenderer.bounds.max.x);
        float randomZ = Random.Range(meshRenderer.bounds.min.z, meshRenderer.bounds.max.z);

        return new Vector3(randomX, meshRenderer.bounds.max.y + 10.0f, randomZ);
    }

    private void SpawnTargetItemDrop(PlayerID playerID)
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        PlayerController playerController = PlayerManager.instance.GetPlayerControllerByID(playerID);
        float coX = playerController.transform.position.x;
        float coZ = playerController.transform.position.z;

        RaycastHit hit;
        if(Physics.Raycast(new Vector3(coX, meshRenderer.bounds.max.y + 11f, coZ), Vector3.down, out hit))
        {
            Vector3 newPos = hit.point;
            newPos.y = 10f;
            Instantiate(RandomItem(), newPos, Quaternion.identity);
        }
    }

    private void RunEvent()
    {
        if(isCheckPlayerThisRound == true)
        {
            List<PlayerController> allPlayerController = PlayerManager.instance.allPlayerController;

            for(int i = 0; i < allPlayerController.Count; i++)
            {
                float chance = KarmaManager.instance.GetBoostModifier(allPlayerController[i].playerStat);
                if(CheckIfDropOnPlayer(chance) == true)
                {
                    SpawnTargetItemDrop(allPlayerController[i].playerStat.playerID);
                    itemPerSec--;
                }
            }

            isCheckPlayerThisRound = false;
        }
        
        for(int i = 0; i < itemPerSecCount; i++)
                SpawnRandomThunder();
    }

    private bool CheckIfDropOnPlayer(float chance)
    {
        int randomNum = Random.Range(1, 101);

        if(randomNum <= Mathf.RoundToInt(chance * 100))
        {
            return true;
        }
        else
        {   
            return false;
        }        
    }

    private GameObject RandomItem()
    {
        return itemList[Random.Range(0, itemList.Length)];
    }
}    

