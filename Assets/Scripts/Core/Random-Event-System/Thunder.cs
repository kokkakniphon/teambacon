﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Thunder : MonoBehaviour
{  
    SpriteRenderer spriteRenderer;
    [SerializeField, BoxGroup("General Info")]
    private float maxDistance = 7.0f;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void StrikeTheThunder()
    {
        RaycastHit[] hits = Physics.BoxCastAll(transform.position, transform.localScale / 2, Vector3.down, transform.rotation, maxDistance);
        if(hits.Length > 0)
        {
            foreach (RaycastHit hit in hits)
            {
                if(hit.collider.gameObject.CompareTag("Player"))
                {
                    hit.collider.gameObject.GetComponent<PlayerController>().Knocked(hit.collider.gameObject.transform.position, 2.0f);
                }
            }
        }
    }

    public void DestroyTheCloud()
    {
        Destroy(this.gameObject);
    }

    private void OnDrawGizmos() 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, Vector3.down * maxDistance);
    }
}
