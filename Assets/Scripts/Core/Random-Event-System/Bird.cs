﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Bird : MonoBehaviour
{
    [SerializeField, BoxGroup("SFX")]
    private AudioData sfx_bird;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            PlayerController player = other.GetComponent<PlayerController>();
            player.Knocked(this.transform.position, 2f);
            AudioManager.instance.Play(sfx_bird);
        }
    }
}
