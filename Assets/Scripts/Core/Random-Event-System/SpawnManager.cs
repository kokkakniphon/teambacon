﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class SpawnManager : MonoBehaviour
{
    [System.Serializable]
    public class ToolList
    {
        [SerializeField]
        public GameObject bat;
        [SerializeField]
        public GameObject bucket;
        [SerializeField]
        public GameObject scythe;
    }

    [SerializeField, InfoBox("Check if you have spawnpoints set only", EInfoBoxType.Normal)]
    private bool randomSpawn = false;
    [ReorderableList]
    public GameObject[] spawnPoints;
    private GameObject lastSpawnPoint;
    public ToolList toolList = new ToolList();

    public static SpawnManager instance;

    private void Awake() 
    {
        if(instance != null)
            Destroy(instance.gameObject);

        instance = this;

        if(randomSpawn == true)
            RandomSpawn();
    }

    public IEnumerator Spawn(Vector3 position, float duration)
    {
        yield return new WaitForSeconds(duration);

        gameObject.transform.position = position;
    }

    void RandomSpawn()
    {
        int randomNumber0 = Random.Range(0, spawnPoints.Length);

        int randomNumber1 = Random.Range(0, spawnPoints.Length);

        while(randomNumber0 == randomNumber1)
            randomNumber1 = Random.Range(0, spawnPoints.Length);

        
        for(int i=0; i < spawnPoints.Length; i++)
        {
            if(i == randomNumber0)
                Instantiate(toolList.bat, spawnPoints[randomNumber0].transform.position, toolList.bat.transform.rotation);
            else if(i == randomNumber1)
                Instantiate(toolList.bat, spawnPoints[randomNumber1].transform.position, toolList.bat.transform.rotation);
            else
                Instantiate(toolList.bucket, spawnPoints[i].transform.position, toolList.bat.transform.rotation);
        }  
    }

    public void Spawn(GameObject gameObject, float duration = 0.0f)
    {
        int randomPos = Random.Range(0, spawnPoints.Length);
        while(spawnPoints[randomPos] == lastSpawnPoint)
        {
            randomPos = Random.Range(0, spawnPoints.Length);
        }

        lastSpawnPoint = spawnPoints[randomPos];

        StartCoroutine(SpawnCoroutine(gameObject, spawnPoints[randomPos].transform.position, duration));
    }

    public void Spawn(GameObject gameObject, Vector3 position, float duration = 0.0f)
    {
        StartCoroutine(SpawnCoroutine(gameObject, position, duration));
    }

    public IEnumerator SpawnCoroutine(GameObject gameObject, Vector3 position, float duration = 0.0f) 
    {   
        if(gameObject.CompareTag("Tool"))
        {
            gameObject.transform.parent = null;
        }
            
        yield return new WaitForSeconds(duration);

        gameObject.transform.position = position;

        if(gameObject.activeSelf != true)
            gameObject.SetActive(true);

        if(gameObject.GetComponent<PlayerController>())
        {
            gameObject.GetComponent<PlayerController>().Blinking();
            gameObject.GetComponent<PlayerController>().ClearStatus();
        }     
    }
}
