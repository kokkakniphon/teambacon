﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[System.Serializable]
public class PlayerAudioList
{
    public AudioData sfx_droping;
    public AudioData sfx_pickup;
    public AudioData sfx_batting;
    public AudioData sfx_buying;
    public AudioData sfx_harvesting;
    public AudioData sfx_openshop;
    public AudioData sfx_planting;
    public AudioData sfx_throwing;
    public AudioData sfx_watering;
    public AudioData sfx_fillBucket;
    public AudioData sfx_playerFall;
}

[System.Serializable]
public class LevelAudioList
{
    public AudioData sfx_startLevel;
    public AudioData sfx_endLevel;
    public AudioData bgm;
}

[System.Serializable]
public class MainMenuAudioList
{
    public AudioData sfx_buttonClick;
    public AudioData bgmMenu;
    public AudioData bgmVictory;
}

public class AudioManager : MonoBehaviour
{   
    public PlayerAudioList playerAudioList = new PlayerAudioList();

    public LevelAudioList levelAudioList = new LevelAudioList();

    public MainMenuAudioList mainMenuAudioList = new MainMenuAudioList();

    public static AudioManager instance;

    private void Awake() 
    {
        if(instance != null)
            Destroy(instance.gameObject);

        instance = this;
    }

    public Coroutine Play(AudioData input, float delay = 0)
    {
        return StartCoroutine(PlayAudio(input, delay));
    }

    private IEnumerator PlayAudio(AudioData audioRef, float delayRef)
    {
        yield return new WaitForSeconds(delayRef);

        GameObject obj = new GameObject("audio_object");
        AudioSource sfx = obj.AddComponent<AudioSource>();

        sfx.clip = audioRef.clip;
        sfx.volume = audioRef.volume;
        sfx.pitch = audioRef.pitch;
        sfx.outputAudioMixerGroup = audioRef.mixer;
        if(audioRef.isLoop == true){sfx.loop = true;}
        else{sfx.loop = false;}
        sfx.Play();
        Destroy(obj, audioRef.clip.length);
    }

    public void StartLeveL()
    {
        Play(levelAudioList.sfx_startLevel);
        Play(levelAudioList.bgm, levelAudioList.sfx_startLevel.clip.length);
    }

    public void EndLevel()
    {
        Play(levelAudioList.sfx_endLevel);
    }

    //Not sure what purpose is this function
    /*private GameObject objRef;
    public void ForceStop()
    {
        if(objRef != null)
            Destroy(objRef);
    }*/
}
