﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "AudioData", menuName = "ScriptableObject/AudioData", order = 4)]

public class AudioData : ScriptableObject
{
    public AudioMixerGroup mixer;
    public AudioClip clip;
    public bool isLoop = false;

    [Range(0,1)]
    public float volume = 1;

    [Range(.25f,3)]
    public float pitch = 1;
}
