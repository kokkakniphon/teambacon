﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ScytheController : Tools
{
    void Awake() 
    {
        base.SetSpawnPoint();

        SetHoldableType(HoldableType.tool);

        toolType = ToolType.scythe;    
    }

    private void Start() 
    {
        onPickUp += OnPickUp;
    }

    private void OnDestroy() 
    {
        onPickUp -= OnPickUp;
    }

    private void OnPickUp()
    {
        transform.rotation = Quaternion.identity;
        this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x) * (this.transform.parent.localScale.x / Mathf.Abs(this.transform.parent.localScale.x)), this.transform.localScale.y, this.transform.localScale.z);
    }
}
