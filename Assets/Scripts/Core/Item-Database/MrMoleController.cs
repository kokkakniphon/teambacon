﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
public class MrMoleController : Items
{
    private Vector3 _direction;
    [SerializeField]
    private float _speed;
    public float speed { get { return _speed;} private set { _speed = value;}}
    private Rigidbody _rb;
    private Coroutine _moleExistence;
    private Coroutine _moleTimer;
    private Coroutine _moleMove;
    private ParticleSystem _ps;
    private ParticleSystem.MainModule _psmain;
    private bool isActivated = false;
    private bool AlreadyUse = false;
    [SerializeField]
    private SpriteRenderer _sr;

    [SerializeField]
    private GameObject _shadow;

    private void Awake()
    {
        _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b, 1f);
        SetHoldableType(HoldableType.item);   
        itemType = ItemType.MrMole;
        _ps = GetComponentInChildren<ParticleSystem>();
        _psmain = _ps.main;
        _ps.Stop();
    }

    private void Start()
    {
        _rb = this.GetComponent<Rigidbody>();
        onPickUp += OnPickUp;
        onPutDown += OnPutDown;
        if(GetComponent<AutoDestroy>())
        {
            onPickUp += GetComponent<AutoDestroy>().OnPickUp;
            onPutDown += GetComponent<AutoDestroy>().OnPutDown;
        }
    }

    [Button]
    private void DestroyMrMole()
    {
        Destroy(this.gameObject);
    }

    void FixedUpdate () 
    {
        if(speed != 0) _rb.velocity = _direction.normalized * speed;   
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Tile" && isActivated == true)
        {
            speed = 0;
            PlantableTile plantableTile = other.GetComponent<PlantableTile>();
            if(plantableTile.GetTileState() != PlantableTile.PlantableTileState.empty)
            {
                if(_moleTimer != null) StopCoroutine(_moleTimer);
                _moleTimer = StartCoroutine(WaitTilFinishEating(plantableTile, GM.instance.itemDB.GetItemDataByID("SI0002").itemDuration));
            }
            else
            {
                //The cause of sliding
                MoveOn();    
            }
            isActivated = true;
            base.isInProcess = true;
        }

        else if (other.CompareTag("Ground") && !other.CompareTag("Tile") && AlreadyUse == false)
        {
            //able to pick up again, reset mole state
            isActivated = false;
            base.isInProcess = false;
        }
    }

    /*private void OnTriggerExit(Collider other) 
    {
        if(other.gameObject.tag == "Tile")
        {
            if(_moleTimer != null)
            {
                StopCoroutine(_moleTimer);
            }
        }
    }*/

    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            DestroyMrMole();
        }
    }

    public override void UseItem(Vector3 lookDir) 
    {
        _ps.Play();
        _psmain.startColor = new Color (0.5660378f, 0.384081f, 0.1308295f);
        if(_moleExistence == null) _moleExistence = StartCoroutine(MoleExistence(30f));
        _direction = lookDir;
        base.UseItem(lookDir);

        isActivated = true;
        base.isInProcess = true;
    }

    IEnumerator WaitTilFinishEating (PlantableTile tile, float duration)
    {
        Debug.Log("Eating.....");
        //_rb.velocity = new Vector3 (0f, 0f, 0f);
        AlreadyUse = true;
        //when eating, set the particle color to yellowish green
        _psmain.startColor = new Color (0.74f, 0.74f, 0.34f);
        _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b, 1f);
        _shadow.SetActive(true);
        //Debug.Log("Eating");
        while(duration > 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }
        //Debug.Log("Finish Eating");
        _psmain.startColor = new Color (0.5660378f, 0.384081f, 0.1308295f);
        _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b, 0f);
        _shadow.SetActive(false);
        tile.ResetTile();
        //Change back to brow when finish eating
        MoveOn();
    }

    IEnumerator MoleExistence(float duration)
    {
        //Start this function after it start eating the first crop
        while(duration > 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }
        DestroyMrMole();
    }

    private void MoveOn()
    {
        //_ps.Play();
        _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b, 0f);
        _shadow.SetActive(false);
        speed = 0.5f;
    }

    private void OnDestroy() 
    {
        if(_moleTimer != null) StopCoroutine(_moleTimer);
        onPickUp -= OnPickUp;
        onPutDown -= OnPutDown;

        if(GetComponent<AutoDestroy>())
        {
            onPickUp -= GetComponent<AutoDestroy>().OnPickUp;
            onPutDown -= GetComponent<AutoDestroy>().OnPutDown;
        }
    }

    private void OnPickUp()
    {
        _ps.Stop();
        speed = 0;
        _sr.color = new Color(_sr.color.r, _sr.color.g, _sr.color.b, 1f);
        if(_moleTimer != null) StopCoroutine(_moleTimer);
        _rb.velocity = _direction * speed;
    }

    private void OnPutDown()
    {
        if(GetComponent<AutoDestroy>() && isActivated == true)
            GetComponent<AutoDestroy>().isActivated = true;
    }
    
}
