﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CloudController : Items
{
    private Rigidbody _rb;
    private Coroutine _cloudTimer;

    private CapsuleCollider capsule;
    public List<PlantableTile> tiles = new List<PlantableTile>();
    
    private bool isActivated = false;
    private Transform cloudSize;

    private void Awake()
    {
        SetHoldableType(HoldableType.item);   
        itemType = ItemType.Cloud;
        cloudSize = this.GetComponent<Transform>();
    }

    private void Start()
    {
        _rb = this.GetComponent<Rigidbody>();
        onPickUp += OnPickUp;
        onPutDown += OnPutDown;
        if(GetComponent<AutoDestroy>() != null)
        {
            onPickUp += GetComponent<AutoDestroy>().OnPickUp;
            onPutDown += GetComponent<AutoDestroy>().OnPutDown;
        }
    }

    IEnumerator DestroyCloud (PlantableTile tile, float duration)
    {
        //set cloud to laction to be on top of the tile
        Transform tileTransform = this.GetComponent<Transform>();
        transform.position = new Vector3(tileTransform.position.x, 1.5f, tileTransform.position.z);
        _rb.isKinematic = true;
        //Set visual
        cloudSize.localScale = new Vector3(2f, 2f, 1f);
        //Countdown and destror self after 10 seconds
        while (duration > 0){
            duration -= Time.deltaTime;
            yield return null;   
        }
        //Debug.Log("Destroy cloud ;-;");
        _rb.isKinematic = false;
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Tile" && isActivated == true)
        {
            capsule = GetComponent<CapsuleCollider>();
            capsule.enabled = true;
            //Debug.Log("On Plantable Tile"); 
            PlantableTile plantableTile = other.GetComponent<PlantableTile>();
            if(plantableTile.GetTileState() != PlantableTile.PlantableTileState.empty)
            {
                isActivated = true;
                base.isInProcess = true;
                UseCloud(plantableTile);
                AddToTile(plantableTile);
                ApplyEffect();
            }
        }
    }

    private void OnDestroy() 
    {
        onPickUp -= OnPickUp;
        onPutDown -= OnPutDown;

        if(GetComponent<AutoDestroy>())
        {
            onPickUp -= GetComponent<AutoDestroy>().OnPickUp;
            onPutDown -= GetComponent<AutoDestroy>().OnPutDown;
        }
    }
    public override void UseItem(Vector3 lookDir) 
    {
        isActivated = true;
    }

    private void OnPickUp()
    {
    }
    private void OnPutDown()
    {
        if(GetComponent<AutoDestroy>() && isActivated == true)
            GetComponent<AutoDestroy>().isActivated = true;
    }

    public void UseCloud(PlantableTile tile)
    {
        if(_cloudTimer != null) StopCoroutine(_cloudTimer);

         _cloudTimer = StartCoroutine(DestroyCloud(tile, GM.instance.itemDB.GetItemDataByID("SI0003").itemDuration));
    }
    
    private void AddToTile(PlantableTile tile)
    {
        foreach (PlantableTile item in tiles)
        {
            if (item.GetInstanceID() == tile.GetInstanceID())
            {
                return;
            }
        }
        tiles.Add(tile);
    }

    private void ApplyEffect()
    {
        foreach (PlantableTile item in tiles)
        {
            Debug.Log("CloudController::ApplyEffect()");
            item.ShadeCrop("SI0003");
        }
    }
}
