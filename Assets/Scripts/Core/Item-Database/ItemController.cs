﻿using NaughtyAttributes;
using UnityEngine;

public enum ItemType
{
    Fertilizer,
    Cloud,
    MrMole, 
    Bat
}

public class Items : Holdable
{
    [SerializeField, BoxGroup("General Info")]
    private ItemData _itemData;
    public ItemData itemData { get { return _itemData; } private set { _itemData = value; }}

    public ItemType itemType;

    public Vector3 lastLookDir;
    public bool isInProcess = false;

    public virtual void UseItem(Vector3 lookDir) 
    {
        lastLookDir = lookDir;
    }

    public void SetItemData(ItemData value_obj)
    {
        itemData = value_obj;
    }

}

public class ItemController : Items
{
    
    private void Awake() 
    {
        SetHoldableType(HoldableType.item);    
    }

    private void Start() 
    {
        onPickUp += OnPickUp;
        onPutDown += OnPutDown;
    }

    private void OnDestroy() 
    {
        onPickUp -= OnPickUp;
        onPutDown -= OnPutDown;
    }

    private void OnPickUp()
    {

    }
    private void OnPutDown()
    {

    }
}
