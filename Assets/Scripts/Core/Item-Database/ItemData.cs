﻿using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "ItemData", menuName = "ScriptableObject/ItemData", order = 2)]


public class ItemData : ScriptableObject
{
    //There are 3 types of items, the category is based on where items use the data.
    /*
    1. Field -> using/affecting data from plantableTile
    2. Crop -> using/affecting crop data (ex. growth rate)
    3. Player -> using/affecting player actions
    */
    public enum Type {Field, Crop, Player, Unspecified};
    [SerializeField, BoxGroup("Item Data Info"), ShowAssetPreview]
    private Sprite _itemIcon;
    public Sprite itemIcon {get {return _itemIcon;} private set{_itemIcon = value;}}

    [SerializeField, BoxGroup("Item Data Info"), ShowAssetPreview]
    private Sprite _inGamePic;
    public Sprite inGamePic {get {return _inGamePic;} private set{_inGamePic = value;}}
    
    
    [SerializeField, BoxGroup("Item Data Info")]
    private string _itemName = "";
    public string itemName {get {return _itemName;} private set{_itemName = value;}}

    [SerializeField, BoxGroup("Item Data Info")]
    public Type itemType = Type.Unspecified;

    [SerializeField, BoxGroup("Item Data Info")]
    private string _itemID = "";
    public string itemID {get {return _itemID;} private set{_itemID = value;}}

    [SerializeField, BoxGroup("Item Data Info")]
    private float _itemDuration = 0;
    public float itemDuration {get {return _itemDuration;} private set{_itemDuration = value;}}

    //Field type items: Shield, Holy Blessing, Mr.Mole
    [SerializeField, BoxGroup("Field")]
    private float _activationDelay = 0;
    public float activationDelay {get {return _activationDelay;} private set{_activationDelay = value;}}

    //Crop type item: Fox Thief, Fertilizer, Locust Swarm, Giant Umbrella
    [SerializeField, BoxGroup("Crop")]
    private float _growthRatePercent = 0;
    public float growthRatePercent {get {return _growthRatePercent;} private set{_growthRatePercent = value;}}

    //Player type item: Green Hands
    [SerializeField, BoxGroup("Player")]
    private float _actionSpeedPercent = 0;
    public float actionSpeedPercent {get {return _actionSpeedPercent;} private set{_actionSpeedPercent = value;}}
}
