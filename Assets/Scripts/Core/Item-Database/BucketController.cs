﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

public enum ToolType
{
    waterBucket,
    scythe,
    bat
}

public class Tools : Holdable
{
    [SerializeField, BoxGroup("General Info")]
    public bool canOnlyUsedOnce = false;
    public float coolDown = 0.0f;
    public ToolType toolType;

    public virtual bool UseTool() => true;

    public virtual void ReloadTool() 
    { 
        if(canOnlyUsedOnce == true)
            DisableAndSpawn();
    }

    [SerializeField, BoxGroup("Respawn")]
    public float respawnDelay = 2.5f;

    [SerializeField, BoxGroup("Respawn")]
    public Vector3 respawnPoint = Vector3.zero;

    public void SetSpawnPoint()
    {
       if(respawnPoint == Vector3.zero)
            respawnPoint = transform.position;
    }

    void SetSpawnPoint(Vector3 vector3)
    {
       if(respawnPoint != vector3)
            respawnPoint = vector3;
    }

    public void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.CompareTag("FallPlain"))
        {
            DisableAndSpawn();
        }      
    }
    
    public void Disable()
    {
        this.gameObject.SetActive(false);
    }

    public void ReSpawn()
    {
        SpawnManager.instance.Spawn(this.gameObject, respawnDelay);
    }

    public void DisableAndSpawn()
    {
        Disable();
        ReSpawn();   
    }

}

public class BucketController : Tools
{
    [SerializeField]
    private int _waterLevel = 5;
    public int waterLevel { get {return _waterLevel;} private set {_waterLevel = value; UpdateIndicator();}}

    [SerializeField]
    private GameObject filledBucket;

    void Awake()
    {
        base.SetSpawnPoint();
        
        SetHoldableType(HoldableType.tool);

        toolType = ToolType.waterBucket;
    }

    private void Start() 
    {
        onPickUp += OnPickUp;
        onPutDown += OnPutDown;

        UpdateIndicator();
    }

    private void OnDestroy() 
    {
        onPickUp -= OnPickUp;
        onPutDown -= OnPutDown;
    }

    public override void ReloadTool()
    {
        waterLevel = 5;
    }

    public override bool UseTool()
    {
        if(waterLevel > 0)
        {
            return true;
        }
        return false;
    }

    public int DecreaseWaterLevel()
    {
        waterLevel--;
        return waterLevel;
    }

    private void OnPickUp()
    {
        transform.rotation = Quaternion.identity;
    }
    private void OnPutDown()
    {

    }

    private void UpdateIndicator()
    {
        filledBucket.SetActive(waterLevel > 0);
    }
}
