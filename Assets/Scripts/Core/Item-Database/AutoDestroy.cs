﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class AutoDestroy : MonoBehaviour
{
    [SerializeField, BoxGroup("General Info")]
    private GameObject spriteGameObject;
    private SpriteRenderer spriteRenderer;
    private Color rendererColor;
    private Color originalColor;

    [SerializeField, BoxGroup("General Info")]
    private float timeBeforeDestroy = 10.0f;
    [SerializeField, BoxGroup("General Info")]
    private float blinkingTime = 2.5f;
    private bool isBlinking = false;
    private bool isPickup = false;
    public bool isActivated = false;
    private void Start() 
    {
        originalColor = spriteGameObject.GetComponent<SpriteRenderer>().color;
        rendererColor = spriteGameObject.GetComponent<SpriteRenderer>().color;
        spriteRenderer = spriteGameObject.GetComponent<SpriteRenderer>();
        StartCoroutine(Countdown(timeBeforeDestroy));
    }

    public void OnPickUp()
    {
        isPickup = true;
    }

    public void OnPutDown()
    {
        isPickup = false;
        if(isActivated != true)
            StartCoroutine(Countdown(timeBeforeDestroy));
    }
    
    IEnumerator Countdown(float duration)
    {
        while(duration > 0)
        {
            duration -= 0.25f;
            
            if(duration <= blinkingTime && isBlinking == false)
                StartCoroutine(BlinkingItem(blinkingTime));
            if(isPickup == true)
                yield break;

            yield return new WaitForSeconds(0.25f);
        }
        Destroy();
    }

    void Destroy()
    {
        Destroy(this.gameObject);
    }

    IEnumerator BlinkingItem(float duration)
    {   
        isBlinking = true;
        while(duration > 0)
        {
            duration -= 0.25f;

            if(isPickup == true)
            {
                spriteRenderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a);
                isBlinking = false;
                yield break;
            }
            else if(spriteRenderer.color.a == originalColor.a)
            {
                spriteRenderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a * 0.25f);
            } 
            else
            {
                spriteRenderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a);
            }

            //How frequent the player blink
            yield return new WaitForSeconds(0.25f);
        }

        spriteRenderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a);
    }
}
