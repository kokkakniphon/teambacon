﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FertilizerController : Items
{    
    private int _count;
    private void Awake()
    {
        _count = 0;
        SetHoldableType(HoldableType.item);   
        itemType = ItemType.Fertilizer;
    }

    private void Start()
    {
        _count = 0;
        if(GetComponent<AutoDestroy>())
        {
            onPickUp += GetComponent<AutoDestroy>().OnPickUp;
            onPutDown += GetComponent<AutoDestroy>().OnPutDown;
        }
    }

    private void OnDestroy() 
    {
        if(GetComponent<AutoDestroy>())
        {
            onPickUp -= GetComponent<AutoDestroy>().OnPickUp;
            onPutDown -= GetComponent<AutoDestroy>().OnPutDown;
        }
    }

    public void FertilizerCount()
    {
        _count++;
        if (_count >= 3){Destroy(this.gameObject);}
    }
}
