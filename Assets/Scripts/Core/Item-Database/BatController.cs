using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class BatController : Tools
{
    [SerializeField]
    private SpriteRenderer batImage;
    bool isOnCoolDown;

    void Awake() 
    {
        base.SetSpawnPoint();
        
        SetHoldableType(HoldableType.tool);

        toolType = ToolType.bat;
    }
    
    private void Start() 
    {
        onPickUp += OnPickUp;
    }

    private void OnDestroy() 
    {
        onPickUp -= OnPickUp;
    }

    public override bool UseTool()
    {
        if(isOnCoolDown == false)
        {
            return true;
        }
        return false;
    }

    [Button]
    public override void ReloadTool()
    {
        if(base.canOnlyUsedOnce == true)
        {
            base.DisableAndSpawn();
        }
        else
        {
            isOnCoolDown = true;
            StartCoroutine(StartCoolDown());
        }
        
    }   

    IEnumerator StartCoolDown()
    {
        batImage.color = Color.red;
        isOnCoolDown = true;
        yield return new WaitForSeconds(coolDown);
        isOnCoolDown = false;
        batImage.color = Color.white;
    }

    private void OnPickUp()
    {
        transform.rotation = Quaternion.identity;
        this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x) 
        * -(this.transform.parent.localScale.x / Mathf.Abs(this.transform.parent.localScale.x)), this.transform.localScale.y, this.transform.localScale.z);
    }
}
