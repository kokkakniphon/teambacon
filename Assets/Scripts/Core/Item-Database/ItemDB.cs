﻿using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "ItemDB", menuName = "DataManager/ItemDatabase", order = 1)]
public class ItemDB : ScriptableObject
{
    [SerializeField, BoxGroup("Crop Database Info"), ReorderableList]
    private List<ItemData> _allItemData = new List<ItemData>();
    public List<ItemData> allItemData { get { return _allItemData; } private set { _allItemData = value; }}

    [SerializeField, BoxGroup("General Settings")]
    private GameObject _itemPrefabs;
    public GameObject itemPrefabs { get => _itemPrefabs; private set { _itemPrefabs = value; }}

    public ItemData GetItemDataByID(string value_string)
    {
        for (int i = 0; i < allItemData.Count; i++)
        {
            if(allItemData[i].itemID == value_string)
                return allItemData[i];
        }

        Debug.LogError($"<b><color=blue>ItemDatabase</color>: Can't fine item data with id: <color=red>{value_string}</color>.</b>");

        return null;
    }
}
