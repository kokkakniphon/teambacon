﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;

public class KarmaStats : MonoBehaviour
{
    [SerializeField, BoxGroup("Karma System")]
    private PlayerID _player_Buy;
    public PlayerID player_Buy { get { return _player_Buy; } private set { _player_Buy = value; }}

    [SerializeField, BoxGroup("Karma System")]
    private PlayerID _player_Plant;
    public PlayerID player_Plant { get { return _player_Plant; } private set { _player_Plant = value; }}

    [SerializeField, BoxGroup("Karma System")]
    private PlayerID _player_Water0;
    public PlayerID player_Water0 { get { return _player_Water0; } private set { _player_Water0 = value; }}

    [SerializeField, BoxGroup("Karma System")]
    private PlayerID _player_Water1;
    public PlayerID player_Water1 { get { return _player_Water1; } private set { _player_Water1 = value; }}

    [SerializeField, BoxGroup("Karma System")]
    private PlayerID _player_Harvest;
    public PlayerID player_Harvest { get { return _player_Harvest; } private set { _player_Harvest = value; }}

    [SerializeField, BoxGroup("Karma System")]
    private PlayerID _player_Sell;
    public PlayerID player_Sell { get { return _player_Sell; } set { _player_Sell = value; }}

    public bool isWaterOnce = false;
    public bool isWaterTwice = false;
    // Karma System
    public void SetPlayerBuy(PlayerID playerID) { player_Buy = playerID; }
    public void SetPlayerPlant(PlayerID playerID) { player_Plant = playerID; }
    public void SetPlayerWater(PlayerID playerID) 
    {
        if(isWaterOnce == false)
        {
            player_Water0 = playerID;
            isWaterOnce = true;
        }
        else if(isWaterTwice == false)
        {
            player_Water1 = playerID;
            isWaterTwice = true;
        }
    }
    public void SetPlayerHarvest(PlayerID playerID) { player_Harvest = playerID; }
    public void SetPlayerSell(PlayerID playerID) { player_Sell = playerID; }
    public void CopyKarmaStats(KarmaStats other)
    {
        player_Buy = other.player_Buy;
        player_Plant = other.player_Plant;
        player_Water0 = other.player_Water0;
        player_Water1 = other.player_Water1;
        player_Harvest = other.player_Harvest;
    }
}
