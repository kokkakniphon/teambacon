﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class KarmaManager : MonoBehaviour
{
    public static KarmaManager instance;

    [SerializeField, ReadOnly, BoxGroup("General Info")]
    private float karmaPoint;

    [SerializeField, BoxGroup ("General Info")]
    private float maxKarmaPoint = 25.0f;

    [SerializeField, BoxGroup ("General Info")]
    private float minKarmaPoint = -25.0f;

    [SerializeField, BoxGroup ("General Info")]
    private int karmaPointLevels = 5;

    private void Awake()
    {
        if(instance != null)
        { Destroy(instance);}
        instance = this;
    }

    public int KarmaPointToLevel(PlayerStat player)
    {
        KarmaPoint karma = player.playerKarma;
        float pointPerLevel = (karma.maxKarmaPoint + Mathf.Abs(karma.minKarmaPoint)) / karmaPointLevels;
        
        if(karma.karmaPoint == maxKarmaPoint)
            return 3;
        else if(karma.karmaPoint == minKarmaPoint)
            return -3;
        else if(karma.karmaPoint >= -(pointPerLevel/2) && karma.karmaPoint <= pointPerLevel/2)
            return 0;
        else if(karma.karmaPoint < -(pointPerLevel/2) && karma.karmaPoint >= -(pointPerLevel/2 + pointPerLevel))
            return -1;
        else if(karma.karmaPoint < -(pointPerLevel/2 + pointPerLevel) && karma.karmaPoint > minKarmaPoint)
            return -2;
        else if(karma.karmaPoint > pointPerLevel/2 && karma.karmaPoint < pointPerLevel/2 + pointPerLevel)
            return 1;
        else
            return 2;
    }

    public float GetSellModifier(PlayerStat player)
    {
        if(KarmaPointToLevel(player) == 0)
            return 1.0f;
        else if(KarmaPointToLevel(player) == -1)
            return 0.75f;
        else if(KarmaPointToLevel(player) == -2)
            return 0.50f;
        else if(KarmaPointToLevel(player) == -3)
            return 0.25f;
        else if(KarmaPointToLevel(player) == 1)
            return 1.25f;
        else if(KarmaPointToLevel(player) == 2)
            return 1.50f;
        else // (KarmaPointToLevel(player) == 3)
            return 2.0f;
    }
    
    public float GetEventModifier(PlayerStat player)
    {
        if(KarmaPointToLevel(player) == -1)
            return 0.2f;
        else if(KarmaPointToLevel(player) == -2)
            return 0.4f;
        else if(KarmaPointToLevel(player) == -3)
            return 0.6f;
        else
            return 0f;
    }

    public float GetBoostModifier(PlayerStat player)
    {
        if(KarmaPointToLevel(player) == 1)
            return 1.15f;
        else if(KarmaPointToLevel(player) == 2)
            return 1.3f;
        else
            return 1.0f;
    }

}
