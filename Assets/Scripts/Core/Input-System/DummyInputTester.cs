﻿using UnityEngine;
using Luminosity.IO;

public class DummyInputTester : MonoBehaviour
{
    private void Update() 
    {
        // Player ONE
        if(InputManager.GetAxisRaw("Horizontal", PlayerID.One) != 0)
            PrintInputDebug("ONE", "Horizontal", InputManager.GetAxisRaw("Horizontal", PlayerID.One));

        if(InputManager.GetAxisRaw("Vertical", PlayerID.One) != 0)
            PrintInputDebug("ONE", "Vertical", InputManager.GetAxisRaw("Vertical", PlayerID.One));

        if(InputManager.GetButtonDown("Interact", PlayerID.One))
            PrintInputDebug("ONE", "Interact");

        // Player TWO
        if(InputManager.GetAxisRaw("Horizontal", PlayerID.Two) != 0)
            PrintInputDebug("TWO", "Horizontal", InputManager.GetAxisRaw("Horizontal", PlayerID.Two));

        if(InputManager.GetAxisRaw("Vertical", PlayerID.Two) != 0)
            PrintInputDebug("TWO", "Vertical", InputManager.GetAxisRaw("Vertical", PlayerID.Two));

        if(InputManager.GetButtonDown("Interact", PlayerID.Two))
            PrintInputDebug("TWO", "Interact");
        
        // Player THREE
        if(InputManager.GetAxisRaw("Horizontal", PlayerID.Three) != 0)
            PrintInputDebug("THREE", "Horizontal", InputManager.GetAxisRaw("Horizontal", PlayerID.Three));

        if(InputManager.GetAxisRaw("Vertical", PlayerID.Three) != 0)
            PrintInputDebug("THREE", "Vertical", InputManager.GetAxisRaw("Vertical", PlayerID.Three));

        if(InputManager.GetButtonDown("Interact", PlayerID.Three))
            PrintInputDebug("THREE", "Interact");
        
        // Player FOUR
        if(InputManager.GetAxisRaw("Horizontal", PlayerID.Four) != 0)
            PrintInputDebug("FOUR", "Horizontal", InputManager.GetAxisRaw("Horizontal", PlayerID.Four));

        if(InputManager.GetAxisRaw("Vertical", PlayerID.Four) != 0)
            PrintInputDebug("FOUR", "Vertical", InputManager.GetAxisRaw("Vertical", PlayerID.Four));

        if(InputManager.GetButtonDown("Interact", PlayerID.Four))
            PrintInputDebug("FOUR", "Interact");
    }

    private void PrintInputDebug(string playerID, string input_name)
    {
        Debug.Log($"<b><color=blue>InputManager</color>: Player <color=green>{playerID}</color> : <color=red>{input_name}</color> button got pressed</b>");    
    }
    private void PrintInputDebug(string playerID, string input_name, float input_value)
    {
        Debug.Log($"<b><color=blue>InputManager</color>: Player <color=green>{playerID}</color> : <color=red>{input_name}</color> is changed to <color=white>{input_value}</color></b>");
    }
}
