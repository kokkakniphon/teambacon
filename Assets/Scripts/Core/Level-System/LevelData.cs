﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObject/LevelData", order = 3)]

public class LevelData : ScriptableObject
{
    [SerializeField, BoxGroup("General")]
    private int _startingMoney = 0;
    public int startingMoney { get {return _startingMoney; } private set{ _startingMoney = value;} }
    [SerializeField, BoxGroup("General")]
    private float _gameTime = 0;
    public float gameTime { get {return _gameTime; } private set{ _gameTime = value;} }

    [SerializeField, BoxGroup("Theme")]
    private string _themeName = "";
    public string themeName { get { return _themeName; } private set{ _themeName = value; } }
    [SerializeField, BoxGroup("Theme")]
    private bool _themeActive = true;
    public bool themeActive { get { return _themeActive; } private set{ _themeActive = value; } }
    [SerializeField, BoxGroup("Theme")]
    private string _themeID;
    public string themeID { get { return _themeID; } private set{ _themeID = value; } }

    [SerializeField, BoxGroup("Level")]
    private string _levelName = "";
    public string levelName { get { return _levelName; } private set{ _levelName = value; } }
    [SerializeField, BoxGroup("Level")]
    private bool _levelActive = true;
    public bool levelActive { get { return _levelActive; } private set{ _levelActive = value; } }
    [SerializeField, BoxGroup("Level")]
    private string _levelID = "";
    public string levelID { get { return _levelID; } private set{ _levelID = value; } }

    [SerializeField, BoxGroup("Unlock Conditions")]
    private int _unlockCostStar = 0;
    public int unlockCostStar { get { return _unlockCostStar; } private set{ _unlockCostStar = value; } }
    [SerializeField, BoxGroup("Unlock Conditions")]
    private int _unlockCostPlayers = 0;
    public int unlockCostPlayers { get { return _unlockCostPlayers; } private set{ _unlockCostPlayers = value; } }

    [SerializeField, BoxGroup("Crops"), ReorderableList]
    private List<CropData> _cropDatas = new List<CropData>();
    public List<CropData> cropDatas { get{ return _cropDatas; } private set{ _cropDatas = value; } }
    // [SerializeField, BoxGroup("Crops"), ReorderableList]
    // private List<int> _basePrice = new List<int>();
    // public List<int> basePrice { get{ return _basePrice; } private set{ _basePrice = value; } }

    [SerializeField, BoxGroup("Items"), ReorderableList]
    private List<ItemData> _itemDatas = new List<ItemData>();
    public List<ItemData> itemDatas { get{ return _itemDatas; } private set{ _itemDatas = value; } }

    [SerializeField, BoxGroup("Player Stats"), ReorderableList]
    private List<PlayerStat> _playerStats = new List<PlayerStat>();
    public List<PlayerStat> playerStats { get{ return _playerStats; } private set{ _playerStats = value; } }

    public void AddPlayerStats(PlayerStat value_stat)
    {
        playerStats.Add(value_stat);
    }

    public void ClearPlayerStats()
    {
        playerStats.Clear();
    }

    [Button]
    public void SortScore()
    {
        if(playerStats.Count > 0)
        {
            bool isDoneSort = false;
            
            while(isDoneSort == false)
            {
                for (int i = 0; i < playerStats.Count; i++)
                {
                    if(i < playerStats.Count - 1)
                    {
                        if(playerStats[i].playerCurrency.softCurrency < playerStats[i+1].playerCurrency.softCurrency)
                        {
                            PlayerStat value = playerStats[i];
                            playerStats[i] = playerStats[i+1];
                            playerStats[i+1] = value;
                            isDoneSort = false;
                            break;
                        }
                    }
                    if(i == playerStats.Count - 1)
                    {
                        isDoneSort = true;
                    }              
                }
            }
        }
    }

}
