﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;

public class UIRewardManager : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private GameObject rewardScreen;

    private void Start()
    {
        LevelManager.instance.onLevelEnd += UpdateInfo;
    }

    private void OnDestroy() {
        LevelManager.instance.onLevelEnd -= UpdateInfo;
    }

    public void UpdateInfo()
    {
        GM.instance.LoadLeaderBoard();
    }
}
