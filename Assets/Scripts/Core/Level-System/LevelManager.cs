﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEngine.Events;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    [SerializeField, BoxGroup("General Settings")]
    private LevelData _levelData;
    public LevelData levelData { get => _levelData; private set { _levelData = value; }}
    public float currentLevelDuration {get; private set;}
    private Coroutine levelTimer;

    [SerializeField, BoxGroup("General Settings")]
    private Animator globalLight;

    public delegate void NoArgs();
    public NoArgs onLevelStart;
    public NoArgs onTimer;
    public NoArgs onLevelEnd;

    [SerializeField, BoxGroup("General Settings")]
    private UnityEvent OnLevelStart;
    [SerializeField, BoxGroup("General Settings")]
    private UnityEvent OnLevelAboutToEnd;

    private bool isAboutToEnd;

    private void Awake() 
    {
        if(instance != null)
            Destroy(instance.gameObject);

        instance = this;
    }

    public void Start()
    {
        StartCoroutine(DelayStartLevel());
    }

    private void SetupGlobalLightDuration()
    {
        globalLight.SetFloat("Duration", 1f/levelData.gameTime);
    }

    IEnumerator DelayStartLevel()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        StartLevel();
    }

    [Button("Start Level")]
    private void StartLevel() 
    {
        currentLevelDuration = levelData.gameTime;
        SetupGlobalLightDuration();

        for (int i = 0; i < PlayerManager.instance.allPlayerStat.Count; i++)
        {
            PlayerManager.instance.allPlayerStat[i].SetSoftCurrency(levelData.startingMoney);
        }

        UIPlayerManager.instance.UpdateUI();

        levelTimer = StartCoroutine(StartLevelTimer());
    }

    [Button("End Level")]
    private void EndLevel()
    {
        levelData.ClearPlayerStats();

        for (int i = 0; i < PlayerManager.instance.allPlayerStat.Count; i++)
        {
            levelData.AddPlayerStats(PlayerManager.instance.allPlayerStat[i]);
        } 

        if(onLevelEnd != null)
            onLevelEnd.Invoke();

        GM.instance.LoadLeaderBoard();
    }

    public IEnumerator StartLevelTimer()
    {
        if(onLevelStart != null)
            onLevelStart.Invoke();
        
        OnLevelStart.Invoke();

        while(currentLevelDuration > 0)
        {
            if(currentLevelDuration < 3f && isAboutToEnd == false)
            {
                isAboutToEnd = true;
                OnLevelAboutToEnd.Invoke();
            }

            currentLevelDuration -= Time.deltaTime;
            if(onTimer != null)
                onTimer.Invoke();
            yield return null;
        }

        EndLevel();
    }

}
