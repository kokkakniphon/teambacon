﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Luminosity.IO;
using NaughtyAttributes;

public class RankingManager : MonoBehaviour
{
    public List<PlayerStat> playerStats = new List<PlayerStat>();
    public static RankingManager instance;
    private void Awake() 
    {
        if(instance != null)
            Destroy(instance.gameObject);

        instance = this;
    }

    private void Start() {
        playerStats = PlayerManager.instance.allPlayerStat;
    }

    [Button]
    public void SortScore()
    {
        if(playerStats.Count > 0)
        {
            bool isDoneSort = false;
            
            while(isDoneSort == false)
            {
                for (int i = 0; i < playerStats.Count; i++)
                {
                    if(i < playerStats.Count - 1)
                    {
                        if(playerStats[i].playerCurrency.softCurrency < playerStats[i+1].playerCurrency.softCurrency)
                        {
                            PlayerStat value = playerStats[i];
                            playerStats[i] = playerStats[i+1];
                            playerStats[i+1] = value;
                            isDoneSort = false;
                            break;
                        }
                    }
                    if(i == playerStats.Count - 1)
                    {
                        isDoneSort = true;
                    }              
                }
            }
        }
    }

    public void AssignRank()
    {
        for(int i = 0; i < playerStats.Count; i++)
        {
            PlayerManager.instance.GetPlayerStatByID(playerStats[i].playerID).SetPlayerRank(i); 
        }
    }
}
