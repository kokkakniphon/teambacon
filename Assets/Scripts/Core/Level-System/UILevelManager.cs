﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;
using System.Collections;

public class UILevelManager : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private Slider levelDurationSlider;

    private void Start()
    {
        LevelManager.instance.onTimer += UpdateInfo;
    }

    private void OnDestroy() {
        LevelManager.instance.onTimer -= UpdateInfo;
    }

    public void UpdateInfo()
    {
        levelDurationSlider.value = 1 - LevelManager.instance.currentLevelDuration / LevelManager.instance.levelData.gameTime;
    }
}
