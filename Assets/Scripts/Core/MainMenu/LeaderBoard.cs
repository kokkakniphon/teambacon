﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderBoard : MonoBehaviour
{
    [SerializeField] // This is just for now
    LevelData levelData;

    [SerializeField]
    private LeaderBoardSlot leaderBoardSlotFirst;
    [SerializeField]
    private LeaderBoardSlot leaderBoardSlotSecond;
    [SerializeField]
    private LeaderBoardSlot leaderBoardSlotThird;
    [SerializeField]
    private LeaderBoardSlot leaderBoardSlotFourth;

    [SerializeField]
    private Sprite rankOne;
    [SerializeField]
    private Sprite rankTwo;
    [SerializeField]
    private Sprite rankThree;
    [SerializeField]
    private Sprite rankFour;

    private void Start()
    {
        levelData.SortScore();
        SetupLeaderBoard();
    }

    private void SetupLeaderBoard()
    {
        leaderBoardSlotFirst.gameObject.SetActive(false);
        leaderBoardSlotSecond.gameObject.SetActive(false);
        leaderBoardSlotThird.gameObject.SetActive(false);
        leaderBoardSlotFourth.gameObject.SetActive(false);

        for (int i = 0; i < levelData.playerStats.Count; i++)
        {
            switch (i)
            {
                case 0:
                    leaderBoardSlotFirst.SetupInfo(levelData.playerStats[i], i+1, rankOne);
                    leaderBoardSlotFirst.gameObject.SetActive(true);
                    break;
                case 1:
                    leaderBoardSlotSecond.SetupInfo(levelData.playerStats[i], i+1, rankTwo);
                    leaderBoardSlotSecond.gameObject.SetActive(true);
                    break;
                case 2:
                    leaderBoardSlotThird.SetupInfo(levelData.playerStats[i], i+1, rankThree);
                    leaderBoardSlotThird.gameObject.SetActive(true);
                    break;
                case 3:
                    leaderBoardSlotFourth.SetupInfo(levelData.playerStats[i], i+1, rankFour);
                    leaderBoardSlotFourth.gameObject.SetActive(true);
                    break;
            }
        }
    }
}
