﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;

public static class TypeControlSchemeManager
{
    public static TypeControlScheme GetControlSchemeFromIndex(int i)
    {
        switch (i)
        {
            case 0:
                return TypeControlScheme.ZeroKeyboard;
            case 1:
                return TypeControlScheme.OneKeyboard;
            case 2:
                return TypeControlScheme.TwoKeyboard;
            case 3:
                return TypeControlScheme.ThreeKeyboard;
            default:
                return TypeControlScheme.TwoKeyboard;
        }
    }
}

public enum TypeControlScheme
{
    ZeroKeyboard,
    OneKeyboard,
    TwoKeyboard,
    ThreeKeyboard
}

[System.Serializable]
public class CharacterSprites
{
    public Sprite character_icon;
    public Sprite character_sprite;
    public RuntimeAnimatorController character_animator;
}

public class CharacterSelectionPage : MonoBehaviour
{
    [SerializeField]
    private CharacterSelectionSlot slotOne;
    [SerializeField]
    private CharacterSelectionSlot slotTwo;
    [SerializeField]
    private CharacterSelectionSlot slotThree;
    [SerializeField]
    private CharacterSelectionSlot slotFour;
    [SerializeField]
    private TextMeshProUGUI gameStartTimer_TMP;

    [SerializeField, BoxGroup("General Settings")]
    private Button changeControlScheme_Button;    
    [SerializeField, BoxGroup("General Settings")]
    private Button startGame_Button;    

    Coroutine gameStartTimer;

    [SerializeField, BoxGroup("General Settings"), ReorderableList]
    private List<CharacterSprites> characterSprites = new List<CharacterSprites>();

    private List<CharacterSprites> currentCharacters = new List<CharacterSprites>();


    private void OnEnable() 
    {
        UpdateSlotControlScheme();

        slotOne.ResetUI();
        slotTwo.ResetUI();
        slotThree.ResetUI();
        slotFour.ResetUI();

        gameStartTimer_TMP.gameObject.SetActive(false);

        CheckIfReadyForStart();
        SetupCharacterSelection();
    }

    private void Start() 
    {
        changeControlScheme_Button.onClick.AddListener(ChangeControlScheme);
        startGame_Button.onClick.AddListener(ReadyForStart);
    }

    private void OnDestroy() 
    {
        changeControlScheme_Button.onClick.RemoveListener(ChangeControlScheme);
        startGame_Button.onClick.RemoveListener(ReadyForStart);
    }

    private void SetupCharacterSelection()
    {
        currentCharacters.Clear();

        foreach (CharacterSprites item in characterSprites)
        {
            currentCharacters.Add(item);
        }
    }

    public int GetClampSelectionValue(int value)
    {
        if(value < 0)
            value = currentCharacters.Count + value;
        return value % currentCharacters.Count;
    }

    public CharacterSprites GetCharacterSprite(int value)
    {
        return currentCharacters[value];
    }

    public bool AttemptConfirmedCharacterSprite(CharacterSprites value)
    {
        if(currentCharacters.Contains(value))
        {
            currentCharacters.Remove(value);
            return true;
        }
        else
        {
            return false;
        }
    }

    public void CheckIfReadyForStart()
    {
        if(slotOne.isConfirmed && slotTwo.isConfirmed && slotThree.isConfirmed && slotFour.isConfirmed)
        {
            ReadyForStart();
        }

        startGame_Button.gameObject.SetActive(GetNumAssigned() >= 2);
    }

    private int GetNumAssigned()
    {
        int num = 0;
        if(slotOne.isConfirmed) num++;
        if(slotTwo.isConfirmed) num++;
        if(slotThree.isConfirmed) num++;
        if(slotFour.isConfirmed) num++;
        return num;
    }

    public void ReadyForStart()
    {
        gameStartTimer_TMP.gameObject.SetActive(true);
        if(gameStartTimer != null) StopCoroutine(gameStartTimer);
        gameStartTimer = StartCoroutine(StartCountDownStartGame(5f));
    }

    public void StopCountDown()
    {
        if(gameStartTimer != null) StopCoroutine(gameStartTimer);
        gameStartTimer_TMP.gameObject.SetActive(false);
    }

    IEnumerator StartCountDownStartGame(float duration)
    {
        gameStartTimer_TMP.text = ((int)duration).ToString();    
        while (duration > 0)
        {            
            yield return new WaitForSeconds(1f);     
            duration -= 1f;
            gameStartTimer_TMP.text = ((int)duration).ToString();       
        }

        PlayerManager.instance.ClearPlayerStat();
        PlayerManager.instance.ClearPlayerController();

        slotOne.SetupPlayerStat();
        slotTwo.SetupPlayerStat();
        slotThree.SetupPlayerStat();
        slotFour.SetupPlayerStat();

        Mainmenu.instance.StartGame();
    }

    [Button]
    private void AssignAll()
    {
        slotOne.AssignedInput();
        slotTwo.AssignedInput();
        slotThree.AssignedInput();
        slotFour.AssignedInput();
    }

    public void ChangeControlScheme()
    {
        GM.instance.currentSchemeIndex++;
        UpdateSlotControlScheme();

        slotOne.ResetUI();
        slotTwo.ResetUI();
        slotThree.ResetUI();
        slotFour.ResetUI();

        StopCountDown();
        CheckIfReadyForStart();

        SetupCharacterSelection();
    }

    private void UpdateSlotControlScheme()
    {
        slotOne.ChangedControlScheme(GM.instance.currentSchemeIndex);
        slotTwo.ChangedControlScheme(GM.instance.currentSchemeIndex);
        slotThree.ChangedControlScheme(GM.instance.currentSchemeIndex);
        slotFour.ChangedControlScheme(GM.instance.currentSchemeIndex);
    }
}
