﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using NaughtyAttributes;
using Luminosity.IO;

public class Pausemenu : MonoBehaviour
{
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnPause;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnResume;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnSelectOptions;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnSelectHowToPlay;

    [SerializeField, BoxGroup("Options")]
    UnityEvent OnSelectAudioSettings;
    [SerializeField, BoxGroup("Options")]
    UnityEvent OnSelectVideoSettings;

    [SerializeField]
    Animator canvasAnim;

    bool isPaused;

    bool isPauseToggleable = true;
    
    public void Pause()
    {
        PlayClickAudio();
        Time.timeScale = 0f;
        OnPause.Invoke();
        isPaused = true;
    }

    public void Resume()
    {
        PlayClickAudio();
        Time.timeScale = 1f;
        OnResume.Invoke();
        isPaused = false;
    }

    private void Update() 
    {
        if(InputManager.GetButtonDown("Pause") && isPauseToggleable == true)
        {
            Debug.Log("Pause");
            StartCoroutine(SetToggleAble());
            if(isPaused == false)
                Pause();
            else
                Resume();
        }    
    }

    IEnumerator SetToggleAble()
    {
        isPauseToggleable = false;
        yield return new WaitForSeconds(0.5f);
        isPauseToggleable = true;
    }

    public void SelectOptions()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectOptions.Invoke();
    }

    public void SelectHowToPlay()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectHowToPlay.Invoke();      
    }
    
    public void SelectAudioSettings()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectAudioSettings.Invoke();       
    }

    public void SelectVideoSettings()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectVideoSettings.Invoke();
    }

    public void PlayOpenAnimation()
    {
        if(canvasAnim != null)
            canvasAnim.SetTrigger("Open");
    }

    public void ReturnToMainMenu()
    {
        Resume();
        GM.instance.LoadMenu();
    }

    public void PlayClickAudio()
    {
        AudioManager.instance.Play(AudioManager.instance.mainMenuAudioList.sfx_buttonClick);
    }
}
