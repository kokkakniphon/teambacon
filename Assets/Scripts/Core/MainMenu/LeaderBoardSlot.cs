﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;

public class LeaderBoardSlot : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private Image boardBackground;
    [SerializeField, BoxGroup("General Settings")]
    private Image characterImage;
    [SerializeField, BoxGroup("General Settings")]
    private Image rank;
    [SerializeField, BoxGroup("General Settings")]
    private TextMeshProUGUI score_TMP;

    public void SetupInfo(PlayerStat playerStat, int rank_value, Sprite rank_sprite)
    {
        if(boardBackground != null) boardBackground.color = playerStat.playerColor;
        characterImage.sprite = playerStat.character.character_sprite;
        rank.sprite = rank_sprite;
        score_TMP.text = playerStat.playerCurrency.softCurrency.ToString();
    }
}
