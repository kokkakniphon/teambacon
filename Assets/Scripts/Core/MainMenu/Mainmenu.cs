﻿using UnityEngine;
using UnityEngine.Events;
using NaughtyAttributes;

public class Mainmenu : MonoBehaviour
{
    public static Mainmenu instance;

    [SerializeField, BoxGroup("Panels")]
    UnityEvent OnEnterPressToStart;
    [SerializeField, BoxGroup("Panels")]
    UnityEvent OnEnterMainMenu;
    [SerializeField, BoxGroup("Panels")]
    UnityEvent OnEnterLeaderBoard;
    [SerializeField, BoxGroup("Panels")]
    UnityEvent OnExitLeaderBoard;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnSelectOptions;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnSelectHowToPlay;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnSelectTutorialPanel;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnSelectLevelSelection;
    [SerializeField, BoxGroup("Buttons")]
    UnityEvent OnSelectCharacterSelection;
    [SerializeField, BoxGroup("Options")]
    UnityEvent OnSelectAudioSettings;
    [SerializeField, BoxGroup("Options")]
    UnityEvent OnSelectVideoSettings;

    string selectedLevel = "";

    Animator anim;
    [SerializeField]
    Animator canvasAnim;

    bool isStarted = false;

    private void Awake() 
    {
        if(instance != null)
            Destroy(instance);

        instance = this;

        anim = GetComponent<Animator>();

        OnEnterPressToStart.Invoke();
        
        isStarted = false;

    }

    private void Start()
    {
        if(GM.instance != null && GM.instance.isFirstTimeAtMenu == false)
        {
            anim.SetTrigger("NotFirstTimeMenu");
            OnEnterMainMenu.Invoke();
        }

        PlayBGM();
    }

    private void Update() 
    {
        if(isStarted == false && Input.anyKeyDown)
        {
            PressStart();
            isStarted = true;
            GM.instance.isFirstTimeAtMenu = false;
        }    
    }

    public void PressStart()
    {
        PlayClickAudio();
        if(anim != null)
            anim.SetTrigger("MainMenu");     
    }

    public void OnEnterMainMenuAnimation()
    {
        PlayOpenAnimation();
        OnEnterMainMenu.Invoke();
    }

    public void SelectOptions()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectOptions.Invoke();
    }

    public void SelectHowToPlay()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectHowToPlay.Invoke();      
    }

    public void SelectLevelSelection()
    {
        PlayClickAudio();
        PlayOpenAnimation();

        if(GM.instance.isFistTimeRunningGame == true)
        {
            //Open tutorial suggesstion.
            OnSelectTutorialPanel.Invoke();
            GM.instance.isFistTimeRunningGame = false;
        }
        else
        {
            //Open normal level selection.
            OnSelectLevelSelection.Invoke();
        }
    }

    public void SelectedLevel(string value_str)
    {
        selectedLevel = value_str;
    }

    public void SelectCharacterSelection()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectCharacterSelection.Invoke();
    }
    
    public void SelectAudioSettings()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectAudioSettings.Invoke();       
    }

    public void SelectVideoSettings()
    {
        PlayClickAudio();
        PlayOpenAnimation();
        OnSelectVideoSettings.Invoke();
    }

    public void PlayOpenAnimation()
    {
        if(canvasAnim != null)
            canvasAnim.SetTrigger("Open");
    }

    public void StartGame()
    {
        if(anim != null)
            anim.SetTrigger("StartGame");
        GM.instance.LoadGame(selectedLevel);
    }

    public void StartGame(string value_str)
    {
        if(anim != null)
            anim.SetTrigger("StartGame");
        GM.instance.LoadGame(value_str);
    }

    [Button]
    public void OnLeaderBoardStart()
    {
        isStarted = true;
        OnEnterLeaderBoard.Invoke();
        if(anim != null)
            anim.SetTrigger("LeaderBoard");
    }

    public void ExitLeaderBoard()
    {
        if(anim != null)
            anim.SetTrigger("MainMenu");
        OnEnterMainMenu.Invoke();
    }

    public void OnExitLeaderBoardAnimation()
    {
        OnExitLeaderBoard.Invoke();
    }

    public void PlayBGM()
    {
        AudioManager.instance.Play(AudioManager.instance.mainMenuAudioList.bgmMenu);
    }

    public void PlayClickAudio()
    {
        AudioManager.instance.Play(AudioManager.instance.mainMenuAudioList.sfx_buttonClick);
    }

    public void Exit()
    {
        PlayClickAudio();
        Application.Quit();
        Debug.Log("Exit");
    }
    
}
