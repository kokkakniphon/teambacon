﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;
using XInputDotNetPure;
using UnityEngine.Events;
using UnityEngine.UI;

public class CharacterSelectionSlot : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private CharacterSelectionPage page;

    [SerializeField, BoxGroup("General Settings")]
    private GameObject notAssignedUI;
    [SerializeField, BoxGroup("General Settings")]
    private GameObject assignedUI;
    [SerializeField, BoxGroup("General Settings")]
    private PlayerID playerID;
    [SerializeField, BoxGroup("General Settings")]
    private PlayerStat playerStat;

    [SerializeField, BoxGroup("General Settings")]
    private UnityEvent onAssigned;

    [SerializeField, BoxGroup("Character Settings")]
    private GameObject notConfirmCharacter;
    [SerializeField, BoxGroup("Character Settings")]
    private GameObject confirmCharacter;
    [SerializeField, BoxGroup("Character Settings")]
    private Image characterImage;

    [SerializeField, BoxGroup("ControlScheme")]
    private GameObject zeroKeyScheme;
    [SerializeField, BoxGroup("ControlScheme")]
    private GameObject oneKeyScheme;
    [SerializeField, BoxGroup("ControlScheme")]
    private GameObject twoKeyScheme;
    [SerializeField, BoxGroup("ControlScheme")]
    private GameObject threeKeyScheme;

    private CharacterSprites characterSprite;

    public bool isAssigned { get; private set;}
    public bool isConfirmed { get; private set;}

    private bool joySelectionValueAvailability;
    private Coroutine joySelectionTimer;

    private int currentSelection = 0;

    public void ResetUI() 
    {
        isAssigned = false;
        isConfirmed = false;
        notAssignedUI.SetActive(true);
        assignedUI.SetActive(false);
    }

    private void Update() 
    {
        if(InputManager.GetAxisRaw("Horizontal", playerID) >= 0.1f || InputManager.GetAxisRaw("Vertical", playerID) >= 0.1f || InputManager.GetAxisRaw("Horizontal", playerID) <= -0.1f || InputManager.GetAxisRaw("Vertical", playerID) <= -0.1f)
        {
            if(isAssigned == false)
            {
                    AssignedInput();
            }    
            else
            {
                if(isConfirmed == false && joySelectionValueAvailability == true)
                {
                    if(joySelectionTimer != null) StopCoroutine(joySelectionTimer);
                    joySelectionTimer = StartCoroutine(JoySelectionTimer());
                    int dir = (InputManager.GetAxisRaw("Horizontal", playerStat.playerID) > 0) ? 1 : -1;
                    currentSelection += dir;
                    currentSelection = page.GetClampSelectionValue(currentSelection);
                    characterSprite = page.GetCharacterSprite(currentSelection);
                    characterImage.sprite = characterSprite.character_sprite;
                }
            }
        }
        else
        {
            joySelectionValueAvailability = true;
        }

        if(isAssigned == true && isConfirmed == false && InputManager.GetButtonDown("Interact", playerID))
        {
            if(page.AttemptConfirmedCharacterSprite(characterSprite) == true)
            {
                ConfirmedSelection();
            }
        }
    }

    private void ConfirmedSelection()
    {
        notConfirmCharacter.SetActive(false);
        confirmCharacter.SetActive(true);
        this.GetComponent<Animator>().SetTrigger("Assigned");

        StartCoroutine(SetVibration(Vector2.one * 2, 0.2f));

        isConfirmed = true;
        page.CheckIfReadyForStart();
    }

    public void SetupPlayerStat()
    {
        if(isConfirmed == true)
        {
            playerStat.SetCharacter(characterSprite);
            PlayerManager.instance.AddPlayerStat(playerStat);
        }
    }

    IEnumerator JoySelectionTimer()
    {
        joySelectionValueAvailability = false;
        yield return new WaitForSeconds(0.5f);
        joySelectionValueAvailability = true;
    }

    [Button]
    public void AssignedInput()
    {
        isAssigned = true;
        assignedUI.SetActive(true);
        notAssignedUI.SetActive(false);

        notConfirmCharacter.SetActive(true);
        confirmCharacter.SetActive(false);

        currentSelection = page.GetClampSelectionValue(currentSelection);
        characterSprite = page.GetCharacterSprite(currentSelection);
        characterImage.sprite = characterSprite.character_sprite;
        
        StartCoroutine(SetVibration(Vector2.one * 2, 0.2f));

        this.GetComponent<Animator>().SetTrigger("Assigned");

        onAssigned.Invoke();
    }

    IEnumerator SetVibration(Vector2 v_value, float duration)
    {
        if(playerStat.IsJoyEnabled == true)
        {
            GamePad.SetVibration(playerStat.playerIndex, v_value.x, v_value.y);
            yield return new WaitForSeconds(duration);
            GamePad.SetVibration(playerStat.playerIndex, 0, 0);
        }
    }

    public void ChangedControlScheme(int n)
    {
        switch (TypeControlSchemeManager.GetControlSchemeFromIndex(n))
        {
            case TypeControlScheme.ZeroKeyboard:
                if(zeroKeyScheme != null) zeroKeyScheme.SetActive(true);
                if(oneKeyScheme != null) oneKeyScheme.SetActive(false);
                if(twoKeyScheme != null) twoKeyScheme.SetActive(false);
                if(threeKeyScheme != null) threeKeyScheme.SetActive(false);

                if(playerStat.playerID == PlayerID.One)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.One);
                }
                else if (playerStat.playerID == PlayerID.Two)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.Two);
                }
                else if (playerStat.playerID == PlayerID.Three)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.Three);
                }
                else if (playerStat.playerID == PlayerID.Four)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.Four);
                }

                break;
            case TypeControlScheme.OneKeyboard:
                if(zeroKeyScheme != null) zeroKeyScheme.SetActive(false);
                if(oneKeyScheme != null) oneKeyScheme.SetActive(true);
                if(twoKeyScheme != null) twoKeyScheme.SetActive(false);
                if(threeKeyScheme != null) threeKeyScheme.SetActive(false);

                if(playerStat.playerID == PlayerID.One)
                {
                    playerStat.IsJoyEnabled = false;
                }
                else if (playerStat.playerID == PlayerID.Two)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.One);
                }
                else if (playerStat.playerID == PlayerID.Three)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.Two);
                }
                else if (playerStat.playerID == PlayerID.Four)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.Three);
                }

                break;
            case TypeControlScheme.TwoKeyboard:
                if(zeroKeyScheme != null) zeroKeyScheme.SetActive(false);
                if(oneKeyScheme != null) oneKeyScheme.SetActive(false);
                if(twoKeyScheme != null) twoKeyScheme.SetActive(true);
                if(threeKeyScheme != null) threeKeyScheme.SetActive(false);

                if(playerStat.playerID == PlayerID.One)
                {
                    playerStat.IsJoyEnabled = false;
                }
                else if (playerStat.playerID == PlayerID.Two)
                {
                    playerStat.IsJoyEnabled = false;
                }
                else if (playerStat.playerID == PlayerID.Three)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.One);
                }
                else if (playerStat.playerID == PlayerID.Four)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.Two);
                }

                break;
            case TypeControlScheme.ThreeKeyboard:
                if(zeroKeyScheme != null) zeroKeyScheme.SetActive(false);
                if(oneKeyScheme != null) oneKeyScheme.SetActive(false);
                if(twoKeyScheme != null) twoKeyScheme.SetActive(false);
                if(threeKeyScheme != null) threeKeyScheme.SetActive(true);

                if(playerStat.playerID == PlayerID.One)
                {
                    playerStat.IsJoyEnabled = false;
                }
                else if (playerStat.playerID == PlayerID.Two)
                {
                    playerStat.IsJoyEnabled = false;
                }
                else if (playerStat.playerID == PlayerID.Three)
                {
                    playerStat.IsJoyEnabled = false;
                }
                else if (playerStat.playerID == PlayerID.Four)
                {
                    playerStat.IsJoyEnabled = true;
                    playerStat.SetPlayerIndex(PlayerIndex.One);
                }

                break;
        }
    }
}
