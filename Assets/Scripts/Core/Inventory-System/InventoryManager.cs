﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Luminosity.IO;

public static class InventoryManager
{
    public static bool AttemptGivePlayerSolfCurrencyByID(PlayerID playerID, int amount)
    {   
        PlayerStat playerStat = PlayerManager.instance.GetPlayerStatByID(playerID);
        if(playerStat == null) return false;

        playerStat.GainSoftCurrency(amount);
        
        UIPlayerManager.instance.UpdateUI();
        
        return true;
    }

    public static bool AttemptReducePlayerSolfCurrencyByID(PlayerID playerID, int amount)
    {   
        PlayerStat playerStat = PlayerManager.instance.GetPlayerStatByID(playerID);
        if(playerStat == null) return false;

        playerStat.LossSoftCurrency(amount);

        UIPlayerManager.instance.UpdateUI();
        
        return true;
    }
    
}
