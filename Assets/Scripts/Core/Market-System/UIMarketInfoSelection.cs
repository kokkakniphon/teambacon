﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;

public class UIMarketInfoSelection : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private Image icon;
    [SerializeField, BoxGroup("General Settings")]
    private TextMeshProUGUI price_TMP;
    [SerializeField, BoxGroup("General Settings")]
    private RectTransform _curserSelection;
    public RectTransform curserSelection { get => _curserSelection; private set { _curserSelection = value; }}

    
    [SerializeField, BoxGroup("Arrow Settings")]
    private Image iconArrow;

    [SerializeField, ShowAssetPreview]
    private Sprite up_red;

    [SerializeField, ShowAssetPreview]
    private Sprite down_green;

    [SerializeField, ShowAssetPreview]
    private Sprite equal;
    private CropData cropData;
    public void Setup(CropData cropData)
    {
        foreach(RectTransform obj in curserSelection)
        {
            Destroy(obj.gameObject);
        }

        this.cropData = cropData;
        icon.sprite = cropData.seedBagImage;
        price_TMP.text = cropData.currentBuyPrice.ToString();
        iconArrow.sprite = equal;
    }

    public void UpdateInfo()
    {
        UpdatePriceArrow();
        icon.sprite = cropData.seedBagImage;
        price_TMP.text = cropData.currentBuyPrice.ToString();
    }

    public void OnCurserIsFocusing(RectTransform curser)
    {
        curser.SetParent(curserSelection, false);
    }
    

    public string GetCropID()
    {
        return cropData.cropID;
    }

    public void UpdatePriceArrow()
    {
        if(MarketManager.instance.BuyPriceDifferent(cropData) > 0)
        {
            iconArrow.sprite = down_green;
        }
        else if(MarketManager.instance.BuyPriceDifferent(cropData) < 0)
        {
            iconArrow.sprite = up_red;
        }
        else
        {
            iconArrow.sprite = equal;
        }
    }
}
