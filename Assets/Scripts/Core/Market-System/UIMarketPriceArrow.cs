﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

public class UIMarketPriceArrow : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private Image icon;

    [SerializeField, ShowAssetPreview]
    private Sprite up;

    [SerializeField, ShowAssetPreview]
    private Sprite down;

    [SerializeField, ShowAssetPreview]
    private Sprite equal;
    
    private CropData cropData;

    public void SetUp(CropData cropData)
    {
        this.cropData = cropData;
        icon.sprite = equal;
    }

    public void UpdateInfoBuy()
    {
        if(MarketManager.instance.BuyPriceDifferent(cropData) > 0)
        {
            icon.sprite = up;
        }
        else if(MarketManager.instance.BuyPriceDifferent(cropData) < 0)
        {
            icon.sprite = down;
        }
        else
        {
            icon.sprite = equal;
        }
    }

    public void UpdateInfoSell()
    {
        if(MarketManager.instance.SellPriceDifferent(cropData) > 0)
        {
            icon.sprite = down;
        }
        else if(MarketManager.instance.SellPriceDifferent(cropData) < 0)
        {
            icon.sprite = up;
        }
        else
        {
            icon.sprite = equal;
        }
    }
}
