﻿using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;

public class MarketController : MonoBehaviour
{

    [SerializeField]
    private AudioData sellingFx;

    public delegate void IDArgs(PlayerID playerID);
    public static IDArgs onSellingCrop;

    public void RequestBuySeedBag(PlayerID playerID, string cropID)
    {
        //Send request to buy seed
        SeedBagController seedBag = Instantiate(GM.instance.cropDB.seedBagPrefabs).GetComponent<SeedBagController>();
        seedBag.SetupSeedBag(cropID);

        if(InventoryManager.AttemptReducePlayerSolfCurrencyByID(playerID, seedBag.cropData.currentBuyPrice) == true)
        {
            PlayerController playerController = PlayerManager.instance.GetPlayerControllerByID(playerID);
            
            playerController.PerformPickUp(seedBag.transform);
            playerController.PerformExitMarket();
            MarketManager.instance.Buy(cropID);

            //Karma
            seedBag.SetPlayerBuyID(playerID);
            PlayerManager.instance.GetPlayerStatByID(playerID).GainKarmaPoint(2.5f);
        }
    }
    public void RequestSellCrop(CropController cropController)
    {
        int netSellPrice = Mathf.FloorToInt(cropController.cropData.currentSellPrice * 
        KarmaManager.instance.GetSellModifier(PlayerManager.instance.GetPlayerStatByID(cropController.lastPlayerID)));

        if(InventoryManager.AttemptGivePlayerSolfCurrencyByID(cropController.lastPlayerID, netSellPrice) == true)
        {
            cropController.Selled();
            AudioManager.instance.Play(sellingFx);

            //Karma
            KarmaStats cropKarma = cropController.transform.GetComponent<KarmaStats>();
            cropKarma.SetPlayerSell(cropController.lastPlayerID);
            if(cropKarma.player_Plant != cropController.lastPlayerID)
            {
                PlayerManager.instance.GetPlayerStatByID(cropController.lastPlayerID).LossKarmaPoint(2.5f);
            }

            if(onSellingCrop != null)
                onSellingCrop.Invoke(cropController.lastPlayerID);
        }
        MarketManager.instance.Sell(cropController.cropData.cropID);
    }

    public void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.CompareTag("Crop") == true)
        {
            CropController cropController = other.gameObject.GetComponent<CropController>();
            RequestSellCrop(cropController);
        }
    }

    //Check if Player have enough money
    public bool IsBuyAble(PlayerID playerID, string cropID)
    {
        int playerSolfCurrency = PlayerManager.instance.GetPlayerStatByID(playerID).playerCurrency.softCurrency; 
        int cropValue = GM.instance.cropDB.GetCropDataByID(cropID).currentBuyPrice;
        if(playerSolfCurrency >= cropValue)
            return true;
        else
            return false;
    }
    
}
