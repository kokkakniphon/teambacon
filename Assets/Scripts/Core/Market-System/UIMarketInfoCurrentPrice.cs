﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;

public class UIMarketInfoCurrentPrice : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private Image icon;
    [SerializeField, BoxGroup("General Settings")]
    private TextMeshProUGUI price_TMP;

    [SerializeField, BoxGroup("Arrow Settings")]
    private Image iconArrow;

    [SerializeField, ShowAssetPreview]
    private Sprite up_green;

    [SerializeField, ShowAssetPreview]
    private Sprite down_red;

    [SerializeField, ShowAssetPreview]
    private Sprite equal;
    private CropData cropData;
    public void Setup(CropData cropData)
    {
        this.cropData = cropData;
        icon.sprite = cropData.cropIcon;
        price_TMP.text = cropData.currentSellPrice.ToString();
        iconArrow.sprite = equal;
    }

    public void UpdateInfo()
    {
        UpdatePriceArrow();
        icon.sprite = cropData.cropIcon;
        price_TMP.text = cropData.currentSellPrice.ToString();
    }

    public void UpdatePriceArrow()
    {
         if(MarketManager.instance.SellPriceDifferent(cropData) > 0)
        {
            iconArrow.sprite = down_red;
        }
        else if(MarketManager.instance.SellPriceDifferent(cropData) < 0)
        {
            iconArrow.sprite = up_green;
        }
        else
        {
            iconArrow.sprite = equal;
        }
    }
}
