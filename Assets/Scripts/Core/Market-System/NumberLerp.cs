﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using NaughtyAttributes;

public class NumberLerp : MonoBehaviour
{
    public enum LerpType
    {
        Constant,
        Graph
    }

    public enum LerpDirection
    {
        Up,
        Down,
        Both
    }

    [Header("General Config")]
    public bool CustomUse = false;
    bool debugMode = false;
    public LerpType lerpType;
    public LerpDirection lerpDirection;
    public float finalValue;
    public float eventsFrequency;

    [Header("TextMeshProUGUI")]
    [Tooltip("Enable if you're using TextMeshProUGUI to read the value.")]
    public bool useTMP = true;
    public TextMeshProUGUI TMPtext;

    [Header("Constant Lerp")]
    [Tooltip("Duration of the waiting time per number.")]
    public float waitPerNum = 0.025f;

    [Header("Graph Lerp")]
    public AnimationCurve graph;
    [Tooltip("Duration of the lerping time.")]
    public float duration;

    [BoxGroup("Lerping Events")]
    [Tooltip("Called at the start of the lerp.")]
    public UnityEvent startLerp;

    [BoxGroup("Lerping Events")]
    [Tooltip("Called each time the number is lerping.")]
    public UnityEvent onLerping;

    [BoxGroup("Lerping Events")]
    [Tooltip("Called at the end of the lerp.")]
    public UnityEvent endLerp;


    public float initialValue;
    bool isLerping;
    [HideInInspector]
    public float lerpingValue;
    float lastFrameValue;
    float lerpTime = 0f;
    float eventFQholder;
    string cleanText;

    private void Awake()
    {
        SetUpVariable();
    }

    private void OnEnable()
    {
        if (!CustomUse)
            Reset();
    }

    string RemoveSpriteIDText(string displayText, bool getSpriteText)
    {
        displayText = Regex.Replace(displayText, @"\s", "");
        Match m = Regex.Match(displayText, @"<[A-Za-z]+=\d+>");
        if (m.Success && !getSpriteText)
        {
            displayText = Regex.Replace(displayText, @m.Value, "");

            return displayText;
        }
        else
        {

            return m.Value;
        }
    }

    private void SetUpVariable()
    {
        if (TMPtext == null)
        {
            TMPtext = GetComponent<TextMeshProUGUI>();
        }
    }

    
    private void SetFinalValue()
    {
        if (TMPtext != null && useTMP)
        {
            try
            {
                string newText = RemoveSpriteIDText(TMPtext.text, false);
                float num;
                float.TryParse(newText, out num);

                if (num != lerpingValue)
                {
                    finalValue = num;
                }
            }
            catch
            {
            }
        }

    }
    
    public void SetFinalValue(float value)
    {
        finalValue = value;
    }

    private void SetTMPText(float x)
    {
        if (TMPtext != null)
        {
            TMPtext.text = RemoveSpriteIDText(TMPtext.text, true) + x.ToString();
        }
    }

    private void Update()
    {
        if (isLerping)
        {
            if (lerpDirection == LerpDirection.Up && lerpingValue < finalValue)
            {
                DoLerp();
            }
            else if (lerpDirection == LerpDirection.Down && lerpingValue > finalValue)
            {
                DoLerp();
            }
            else if (lerpDirection == LerpDirection.Both)
            {
                DoLerp();
            }
            else
            {
                lerpingValue = finalValue;
                FinishLerp();
            }

        }

        if (!isLerping)
        {
            if (initialValue != finalValue)
            {
                isLerping = true;
                startLerp.Invoke();
                if (debugMode)
                {
                    Debug.Log("StartLerp");
                }
            }
        }
    }

    void DoLerp()
    {
        float interpolartionV = 0f;
        // Lerping to the final number base on the selected lerp type.
        if (lerpType == LerpType.Constant)
        {
            // Set the devide duration to the amount of num diffrence multiply by the time between num.
            lerpTime += Time.deltaTime * (1 / (waitPerNum * Mathf.Abs(finalValue - initialValue)));
            interpolartionV = lerpTime;
        }
        else if (lerpType == LerpType.Graph)
        {
            // Set the devide duration to the set duration.
            lerpTime += Time.deltaTime * (1 / duration);
            interpolartionV = graph.Evaluate(lerpTime);
        }



        // Lerping from start value to the final value.
        lerpingValue = (Int32)(Mathf.Lerp(initialValue, finalValue, interpolartionV));

        if (debugMode)
        {
            Debug.Log("OnLerping");
        }

        eventFQholder += Mathf.Abs(lerpingValue - lastFrameValue);

        if (eventFQholder >= eventsFrequency)
        {
            if (debugMode)
            {
                Debug.Log("OnTrigger " + eventFQholder);
            }
            eventFQholder = 0;
            onLerping.Invoke();
        }

        SetTMPText(lerpingValue);

        // If the value rich the final value set everything back to setup state;
        if (lerpingValue == finalValue)
        {
            FinishLerp();
        }

        lastFrameValue = lerpingValue;
    }

    void FinishLerp()
    {
        Reset();
        endLerp.Invoke();
        if (debugMode)
        {
            Debug.Log("EndLerp");
        }
    }

    private void Reset()
    {
        finalValue = lerpingValue;
        initialValue = lerpingValue;
        SetTMPText(lerpingValue);
        lerpTime = 0;
        isLerping = false;
    }

    public void ReadTMP()
    {
        if (TMPtext != null)
        {
            initialValue = Int32.Parse(TMPtext.text);
        }
    }
}
