﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using Luminosity.IO;
using TMPro;

[RequireComponent(typeof(MarketController))]
public class UIMarket : MonoBehaviour
{
    public static UIMarket instance;

    [SerializeField, BoxGroup("Info Selection Settings")]
    private RectTransform uiMarketInfoSection;
    [SerializeField, BoxGroup("Info Selection Settings")]
    private GameObject uiMarketInfo_obj;
    [SerializeField, BoxGroup("Info Selection Settings")]
    private GameObject uiMarketCurser_obj;

    [SerializeField, BoxGroup("Info Selection Settings")]
    private GameObject uiMarketBuy;

    [SerializeField, BoxGroup("Info Current Price Settings")]
    private RectTransform uiMarketInfoCurrentPriceSection;
    [SerializeField, BoxGroup("Info Current Price Settings")]
    private GameObject uiMarketInfoCurrentPrice_obj;
    [SerializeField, BoxGroup("Info Current Price Settings")]
    private Slider uiTimerSlider;
    [SerializeField, BoxGroup("Info Current Price Settings")]
    private TextMeshProUGUI uiTimerTMP;

    private MarketController marketController;

    private List<UIMarketInfoSelection> uIMarketInfoSelections = new List<UIMarketInfoSelection>();
    private List<UIMarketCurser> cursers = new List<UIMarketCurser>();

    private List<UIMarketInfoCurrentPrice> uIMarketInfoCurrentPrices = new List<UIMarketInfoCurrentPrice>();

    private bool protectArrowChange = false;

    private void Awake() 
    {
        if(instance != null)
            Destroy(instance.gameObject);

        instance = this;

        marketController = this.GetComponent<MarketController>();
    }

    private void Start() 
    {
        LevelManager.instance.onLevelStart += Setup;
        MarketManager.instance.onUpdateMarketPrice += UpdateMarketInfo;
    }

    private void OnDestroy() 
    {
        LevelManager.instance.onLevelStart -= Setup;
        MarketManager.instance.onUpdateMarketPrice -= UpdateMarketInfo;
    }

    public void Setup()
    {

        List<CropData> cropDatas = new List<CropData>();

        foreach (CropData crop in LevelManager.instance.levelData.cropDatas)
        {
            cropDatas.Add(crop);
        }

        while(cropDatas.Count > 3)
        {
            cropDatas.RemoveAt(Random.Range(0, cropDatas.Count));
        }

        foreach(RectTransform obj in uiMarketInfoSection)
        {
            Destroy(obj.gameObject);
        }

        for (int i = 0; i < cropDatas.Count; i++)
        {        
            UIMarketInfoSelection infoSelection = Instantiate(uiMarketInfo_obj, uiMarketInfoSection).GetComponent<UIMarketInfoSelection>();
            infoSelection.Setup(cropDatas[i]);
            uIMarketInfoSelections.Add(infoSelection);
        }

        foreach(RectTransform obj in uiMarketInfoCurrentPriceSection)
        {
            Destroy(obj.gameObject);
        }

        for (int i = 0; i < cropDatas.Count; i++)
        {        
            UIMarketInfoCurrentPrice infoCurrentPrice = Instantiate(uiMarketInfoCurrentPrice_obj, uiMarketInfoCurrentPriceSection).GetComponent<UIMarketInfoCurrentPrice>();
            infoCurrentPrice.Setup(cropDatas[i]);
            uIMarketInfoCurrentPrices.Add(infoCurrentPrice);
        }
        
        for (int i = 0; i < PlayerManager.instance.allPlayerStat.Count; i++)
        {
            UIMarketCurser marketCurser = Instantiate(uiMarketCurser_obj, uIMarketInfoSelections[0].curserSelection).GetComponent<UIMarketCurser>();
            marketCurser.Setup(PlayerManager.instance.allPlayerStat[i]);
            cursers.Add(marketCurser);
        }
    }

    public void MoveCurser(PlayerID playerID, int dir)
    {
        for (int i = 0; i < cursers.Count; i++)
        {
            if(cursers[i].CheckPlayerID(playerID) == true)
            {
                cursers[i].pos += dir;
                if(cursers[i].pos > uIMarketInfoSelections.Count-1)
                    cursers[i].pos = uIMarketInfoSelections.Count-1;
                uIMarketInfoSelections[cursers[i].pos].OnCurserIsFocusing(cursers[i].rectTransform);
            }
        }
    }

    public void UpdateMarketInfo()
    {
        //Temp Bug Fix
        if(protectArrowChange == false)
        {
            protectArrowChange = true;
            return;
        }
        
        for (int i = 0; i < uIMarketInfoSelections.Count; i++)
        {
            uIMarketInfoSelections[i].UpdateInfo();
        }

        for (int i = 0; i < uIMarketInfoCurrentPrices.Count; i++)
        {
            uIMarketInfoCurrentPrices[i].UpdateInfo();
        }

    }

    public void PlayerEnterMarket(PlayerID playerID)
    {
        for (int i = 0; i < cursers.Count; i++)
        {
            if(cursers[i].CheckPlayerID(playerID) == true)
            {
               cursers[i].SetIsUsing(true);
               CheckUIMarketBuy();
            }
        }
    }

    public void PlayerExitMarket(PlayerID playerID)
    {
        for (int i = 0; i < cursers.Count; i++)
        {
            if(cursers[i].CheckPlayerID(playerID) == true)
            {
               cursers[i].SetIsUsing(false);
               CheckUIMarketBuy();
            }
        }
    }

    public bool PlayerAttemptBuy(PlayerID playerID)
    {
        for (int i = 0; i < cursers.Count; i++)
        {
            if(cursers[i].CheckPlayerID(playerID) == true)
            {
                string cropID = uIMarketInfoSelections[cursers[i].pos].GetCropID();
                if(marketController.IsBuyAble(playerID, cropID) == false) break;
                marketController.RequestBuySeedBag(playerID, cropID);
                return true;
            }
        }

        return false;
    }

    public void UpdateMarketPriceTimer(float value, int timeLeft)
    {
        uiTimerSlider.value = value;
        uiTimerTMP.text = timeLeft.ToString();
    } 

    public void CheckUIMarketBuy()
    {
        Canvas uiMarketBuyStatus = uiMarketBuy.GetComponent<Canvas>();         
        bool occupy = false;

        for (int i = 0; i < cursers.Count; i++)
        {
            if(cursers[i].isUsing == true)
            {
                occupy = true;
                break;
            }    
        }

        if(occupy == true && uiMarketBuyStatus.enabled == false)
        {
            uiMarketBuyStatus.enabled = true;
        }   
        else if(occupy == false && uiMarketBuyStatus.enabled == true)
        {
            uiMarketBuyStatus.enabled = false;
        }       
    }
}
