﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class MarketManager : MonoBehaviour //Buying
{
    public static MarketManager instance;

    [SerializeField]
    private AudioData priceChangeFx;
    
    [System.Serializable]
    public class BuyItem
    {   
        [SerializeField, ReadOnly]
        private string itemID;
        [SerializeField, ReadOnly]
        private int itemBought = 0;
        [SerializeField, ReadOnly]
        private int itemPriceOld = 0;
        //Constructor
        public BuyItem(string inputID)
        {
            itemID = inputID;
        }

        public string GetItemID()
        {
            return itemID;
        }

        public void SetItemBought(int input)
        {
            itemBought = input;
        }

        public int GetItemBought()
        {
            return itemBought;
        }

        public void AddItemBought(int amount)
        {
            itemBought += amount;
        }

        public void SetItemPriceOld(int input)
        {
            itemPriceOld = input;
        }

        public int GetItemPriceOld()
        {
            return itemPriceOld;
        }

    }

    [System.Serializable]
    public class SellItem
    {
        [SerializeField, ReadOnly]
        private string itemID;
        [SerializeField, ReadOnly]
        private int itemSold = 0;

        [SerializeField, ReadOnly]
        private int itemPriceOld = 0;

        //Constructor
        public SellItem(string inputID)
        {
            itemID = inputID;
        }

        public string GetItemID()
        {
            return itemID;
        }

        public void SetItemSold(int input)
        {
            itemSold = input;
        }

        public int GetItemSold()
        {
            return itemSold;
        }

        public void AddItemSold(int amount)
        {
            itemSold += amount;
        }

        public void SetItemPriceOld(int input)
        {
            itemPriceOld = input;
        }

        public int GetItemPriceOld()
        {
            return itemPriceOld;
        }

    }

    [SerializeField, BoxGroup("General")]
    public float marketDuration;
    public float marketCurrentDuration;
    private Coroutine marketTimer;

    [SerializeField]
    List<BuyItem> BuyItems = new List<BuyItem>();
    [SerializeField]
    List<SellItem> SellItems = new List<SellItem>();

    public delegate void NoArgs();
    public NoArgs onUpdateMarketPrice;
    

    private void Awake()
    {
        if(instance != null)
        { Destroy(instance);}
        instance = this;
    }

    void Start()
    {
        LevelManager.instance.onLevelStart += SetupMarketItems;
        LevelManager.instance.onLevelEnd += StopMarketTimer;
    }

    private void OnDestroy() 
    {
        LevelManager.instance.onLevelStart -= SetupMarketItems;
        LevelManager.instance.onLevelEnd -= StopMarketTimer;
    }

    private void SetupMarketItems()
    {
        List<CropData> cropDatas = new List<CropData>();
        
        foreach (CropData crop in LevelManager.instance.levelData.cropDatas)
        {
            cropDatas.Add(crop);
        }

        /*while(cropDatas.Count > 3)
        {
            cropDatas.RemoveAt(Random.Range(0, cropDatas.Count));
        }*/

        for (int i = 0; i < cropDatas.Count; i++)
        {
            CropData cropData = cropDatas[i];
            cropData.SetCurrentBuyPrice(cropData.baseBuyPrice);
            Buy(cropData.cropID);
            BuyItems[i].SetItemBought(0);
            cropData.SetCurrentSellPrice(cropData.baseSellPrice);
            Sell(cropData.cropID);
            SellItems[i].SetItemSold(0);
        }
        StartMarketTimer();
    }

    //Buy Functions

    public BuyItem GetBuyItemByID(string itemID)
    {
        for(int i = 0; i < BuyItems.Count; i++)
        {
            if(BuyItems[i].GetItemID() == itemID)
            {
                return BuyItems[i];
            }
        }

        return null;
    }

    public void Buy(string itemID)
    {
        for (int i = 0; i < BuyItems.Count; i++)
        {
            if(BuyItems[i].GetItemID() == itemID)
            {
                BuyItems[i].AddItemBought(1);
                return;
            }
        }

        BuyItem buyItem = new BuyItem(itemID);
        buyItem.SetItemBought(1);
        BuyItems.Add(buyItem);
    }

    public void ChangeBuyPriceAll()
    {
        int itemBoughtAll = ItemBoughtAll();
        for (int i = 0; i < BuyItems.Count; i++)
        {   
            CropData cropData = GM.instance.cropDB.GetCropDataByID(BuyItems[i].GetItemID());
            cropData.SetCurrentBuyPrice(Mathf.RoundToInt(cropData.baseBuyPrice * BuyModifier(BuyItems[i].GetItemBought(), itemBoughtAll)));
        }
    }

    public float BuyModifier(int itemBought, int itemBoughtAll)
    {
        float temp = itemBought / (float)itemBoughtAll * 100;

        if (temp == 0.0)
            return 0.8f;
        else if (temp >= 1.0 && temp <= 30.0)
            return 1.0f;
        else if (temp > 30.0 && temp <= 50.0)
            return 1.15f;
        else if (temp > 50.0 && temp <= 60.0)
            return 1.25f;
        else
            return 1.5f;
    }

    public int ItemBoughtAll()
    {
        int result = 0;

        for (int i = 0; i < BuyItems.Count; i++)
        {
            result += BuyItems[i].GetItemBought();
        }

        if (result == 0) result = 1;
        return result;
    }

    public void ResetItemBought()
    {
        for (int i = 0; i < BuyItems.Count; i++)
        {
            BuyItems[i].SetItemBought(0);
        }
    }

    //Selling Functions

    public SellItem GetSellItemByID(string itemID)
    {
        for(int i = 0; i < SellItems.Count; i++)
        {
            if(SellItems[i].GetItemID() == itemID)
            {
                return SellItems[i];
            }
        }

        return null;
    }
    public void Sell(string itemID)
    {
        for (int i = 0; i < SellItems.Count; i++)
        {
            if(SellItems[i].GetItemID() == itemID)
            {
                SellItems[i].AddItemSold(1);
                return;
            }
        }

        SellItem sellItem = new SellItem(itemID);
        sellItem.SetItemSold(1);
        SellItems.Add(sellItem);
    }

    public void ChangeSellPriceAll()
    {
        int itemSoldAll = ItemSoldAll();
        for (int i = 0; i < SellItems.Count; i++)
        {
            CropData cropData = GM.instance.cropDB.GetCropDataByID(SellItems[i].GetItemID());
            cropData.SetCurrentSellPrice(Mathf.RoundToInt(cropData.baseSellPrice * SellModifier(SellItems[i].GetItemSold(), itemSoldAll)));
        }
    }

    public float SellModifier(int itemSold, int itemSoldAll)
    {
        float sellPer = itemSold / (float)itemSoldAll * 100;

        if(sellPer < 30)
            return 1 + (0.5f - (sellPer / 60)); 
        else if (sellPer == 100)
            return 0.7f;
        else if(sellPer > 50)
            return 1 - (0.3f - (100 - sellPer) / 50)  *  0.3f;
        else
            return 1;
    }

    public int ItemSoldAll()
    {
        int result = 0;

        for (int i = 0; i < SellItems.Count; i++)
        {
            result += SellItems[i].GetItemSold();
        }

        if (result == 0) result = 1;
        return result;
    }

    public void ResetItemSold()
    {
        for (int i = 0; i < SellItems.Count; i++)
        {
            SellItems[i].SetItemSold(0);
        }
    }

    public void StartMarketTimer()
    {
        //Random new marketTimer
        marketDuration = Random.Range(40, 121);
        marketCurrentDuration = marketDuration;

        if(onUpdateMarketPrice != null) onUpdateMarketPrice.Invoke();

        marketTimer = StartCoroutine(MarketTimer());
    }

    public void StopMarketTimer()
    {
        if(marketTimer != null)
            StopCoroutine(marketTimer);
    }

    //Market
    IEnumerator MarketTimer()
    {
        while(marketCurrentDuration > 0)
        {
            marketCurrentDuration -= Time.deltaTime;
            UIMarket.instance.UpdateMarketPriceTimer(marketCurrentDuration/marketDuration, (int)marketCurrentDuration);
            yield return null;
        }

        AudioManager.instance.Play(priceChangeFx);

        //Buying
        StoreBuyItemPriceOld();
        ChangeBuyPriceAll();
        ResetItemBought();

        //Sell
        StoreSellItemPriceOld();
        ChangeSellPriceAll();
        ResetItemSold();

        StartMarketTimer();
    }

    public int BuyPriceDifferent(CropData cropData)
    {
        int newPrice = cropData.currentBuyPrice;
        BuyItem buyItem = GetBuyItemByID(cropData.cropID);
        int oldPrice = buyItem.GetItemPriceOld();

        return oldPrice - newPrice;
    }

    public int SellPriceDifferent(CropData cropData)
    {
        int newPrice = cropData.currentSellPrice;
        SellItem sellItem = GetSellItemByID(cropData.cropID);
        int oldPrice = sellItem.GetItemPriceOld();

        return oldPrice - newPrice;
    }

    public void StoreBuyItemPriceOld()
    {
        for (int i = 0; i < BuyItems.Count; i++)
        {
            CropData cropData = GM.instance.cropDB.GetCropDataByID(BuyItems[i].GetItemID());
            BuyItems[i].SetItemPriceOld(cropData.currentBuyPrice);
        }
    }

    public void StoreSellItemPriceOld()
    {
        for (int i = 0; i < SellItems.Count; i++)
        {
            CropData cropData = GM.instance.cropDB.GetCropDataByID(SellItems[i].GetItemID());
            SellItems[i].SetItemPriceOld(cropData.currentSellPrice);
        }
    }
    
}