﻿using UnityEngine;
using UnityEngine.UI;
using Luminosity.IO;

public class UIMarketCurser : MonoBehaviour
{
    [SerializeField]
    private Image icon;

    [SerializeField]
    private int _pos;
    public int pos { get => _pos; set { if(value<=0){_pos = 0;}else{_pos = value;}}}
    public RectTransform rectTransform;

    public bool isUsing { get; private set; }

    private PlayerStat playerStat;

    public void Setup(PlayerStat playerStat)
    {
        this.playerStat = playerStat;
        icon.sprite = playerStat.character.character_icon;
        pos = 0;
        rectTransform = this.GetComponent<RectTransform>();
        UpdateInfo();
    }

    public void SetIsUsing(bool value_bool) { isUsing = value_bool; UpdateInfo();}

    public bool CheckPlayerID(PlayerID playerID)
    {
        return (playerStat.playerID == playerID);
    }

    public void UpdateInfo()
    {
        this.gameObject.SetActive(isUsing);
    }
}
