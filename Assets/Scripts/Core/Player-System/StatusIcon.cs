﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class StatusIcon : MonoBehaviour
{   
    [System.Serializable]
    public class IconList
    {
        [SerializeField, ShowAssetPreview]
        public Sprite exclamation;
        [SerializeField, ShowAssetPreview]
        public Sprite stun;
        [SerializeField, ShowAssetPreview]
        public Sprite devil;
        [SerializeField, ShowAssetPreview]
        public Sprite halo;
        [SerializeField, ShowAssetPreview]
        public Sprite blank;

    }
    
    private SpriteRenderer _spriteRenderer;
    private float _timer;
    private Coroutine _showSign;

    public IconList iconList = new IconList();
    private Sprite lastSprite;

    void Awake()
    {
        _spriteRenderer = this.GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = iconList.blank;
    }

    public void Show(Sprite icon,float duration)
    {
        
        lastSprite = _spriteRenderer.sprite;
        
        if(_showSign != null) StopCoroutine(_showSign);
        _spriteRenderer.sprite = icon;
        _showSign = StartCoroutine(ShowStatus(duration));
    }
    public void Show(Sprite icon)
    {
        _spriteRenderer.sprite = icon;
    }
    public void ClearShow()
    {
        _spriteRenderer.sprite = iconList.blank;
    }

    IEnumerator ShowStatus(float duration)
    {
        _timer = duration;

        while(_timer > 0)
        {
            _timer -= Time.deltaTime;
            yield return null;
        }

        /*if(_spriteRenderer.sprite != lastSprite)
            _spriteRenderer.sprite = lastSprite;*/
    }

    public Sprite GetCurrentIcon()
    {
        return _spriteRenderer.sprite;
    }
}
