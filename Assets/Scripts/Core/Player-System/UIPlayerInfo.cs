﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;
using Luminosity.IO;

public class UIPlayerInfo : MonoBehaviour
{
    [SerializeField, BoxGroup("General Settings")]
    private PlayerID playerID;
    [SerializeField, BoxGroup("General Settings")]
    private Image characterIcon;
    [SerializeField, BoxGroup("General Settings")]
    private TextMeshProUGUI solfCurrency_TMP;

    [SerializeField, BoxGroup("Karma Settings")]
    private Slider karma_Slider;
    [SerializeField, BoxGroup("Karma Settings")]
    private TextMeshProUGUI karmaPoint;

    [SerializeField, BoxGroup("Ranking")]
    private Image player_rank;
    private PlayerStat playerStat;
    private NumberLerp numberLerp;
    
    public void Setup()
    {
        if(PlayerManager.instance.IsPlayerStatContainPlayerID(playerID))
        {
            playerStat = PlayerManager.instance.GetPlayerStatByID(playerID);
            numberLerp = solfCurrency_TMP.GetComponent<NumberLerp>();
            numberLerp.ReadTMP();
            this.gameObject.SetActive(playerStat != null);

            UpdateInfo();
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void UpdateInfo()
    {
        if(playerStat != null)
        {
            this.gameObject.SetActive(true);
            // characterIcon.gameObject.SetActive(true);
            solfCurrency_TMP.gameObject.SetActive(true);
            characterIcon.sprite = playerStat.character.character_sprite;
            solfCurrency_TMP.text = playerStat.playerCurrency.softCurrency.ToString();
            numberLerp.SetFinalValue(playerStat.playerCurrency.softCurrency);

            if(karma_Slider != null && karmaPoint != null)
            {
                karma_Slider.gameObject.SetActive(true);
                karmaPoint.gameObject.SetActive(true);

                karma_Slider.value = playerStat.playerKarma.karmaPoint;
                karmaPoint.text = "X " + KarmaManager.instance.GetSellModifier(playerStat);
            }

            if(player_rank != null) 
            {
                player_rank.gameObject.SetActive(true);

                switch (PlayerManager.instance.GetPlayerStatByID(playerID).playerRank)
                {
                    case 0:
                        player_rank.sprite = UIPlayerManager.instance.rank_Sprites.rankOne;
                        break;
                    case 1:
                        player_rank.sprite = UIPlayerManager.instance.rank_Sprites.rankTwo;
                        break;
                    case 2:
                        player_rank.sprite = UIPlayerManager.instance.rank_Sprites.rankThree;
                        break;
                    case 3:
                        player_rank.sprite = UIPlayerManager.instance.rank_Sprites.rankFour;
                        break;
                }
                
            }

        }
    }
}
