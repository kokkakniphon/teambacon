﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantableTileDetector : MonoBehaviour
{
    private bool isDebugging = false;

    public int focusAmount = 1;
    public Transform centerTrans;

    private List<PlantableTile> detectedPlantableTiles = new List<PlantableTile>();
    public PlantableTile focusedPlantableTiles {get; private set;}
    
    private void AddPlantableTile(PlantableTile value_obj)
    {
        // Check for duplication
        for (int i = 0; i < detectedPlantableTiles.Count; i++)
        {
            if(value_obj.name == detectedPlantableTiles[i].name)
                return;
        }

        detectedPlantableTiles.Add(value_obj);
    }

    private void RemovePlantableTile(PlantableTile value_obj)
    {
        detectedPlantableTiles.Remove(value_obj);
    }

    private void SetFocusPlantableTile()
    {
        int nearestTile = -1;
        float nearestDis = -1;
        for (int i = 0; i < detectedPlantableTiles.Count; i++)
        {
            float dis = Vector3.Distance(detectedPlantableTiles[i].gameObject.transform.position, centerTrans.position);

            if(i == 0 || dis < nearestDis)
            {
                nearestTile = i;
                nearestDis = dis;
            }
        }

        if(nearestTile == -1 && focusedPlantableTiles != null)
        {
            focusedPlantableTiles.FocusingOnTile(false);
            focusedPlantableTiles = null;
        }

        if(focusedPlantableTiles != null)
        {
            if(focusedPlantableTiles != detectedPlantableTiles[nearestTile])
                focusedPlantableTiles.FocusingOnTile(false);
        }

        if(nearestTile != -1)
        {
            focusedPlantableTiles = detectedPlantableTiles[nearestTile];
            focusedPlantableTiles.FocusingOnTile(true);
        }
    }

    public bool IsOnPlantableTile() => (focusedPlantableTiles != null);

    public bool AttemptPlantCrop(CropData value_crop, Luminosity.IO.PlayerID playerID)
    {
        if(focusedPlantableTiles != null)
        {
            return focusedPlantableTiles.AttemptPlantCrop(value_crop, playerID);
        }
        else
        {
            return false;
        }
    }

    public bool AttemptWatering()
    {
        if(focusedPlantableTiles != null)
        {
            focusedPlantableTiles.WaterTile();
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool AttemptHarvesting(Luminosity.IO.PlayerID playerID)
    {
        if(focusedPlantableTiles != null)
        {
            return focusedPlantableTiles.AttemptHarvestCrop(playerID);
        }
        else
        {
            return false;
        }
    }

    public bool AttemptFertilizeCrop(string itemID)
    {
        if(focusedPlantableTiles != null)
        {
            focusedPlantableTiles.FertilizeCrop(itemID);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool AttemptShadeCrop(string itemID)
    {
        if(focusedPlantableTiles != null)
        {
            Debug.Log("Attempt Shade Crop");
            focusedPlantableTiles.ShadeCrop(itemID);
            return true;
        }
        else
        {
            return false;
        }  
    }

    public void MrMoleEats()
    {
        if(focusedPlantableTiles != null)
        {
            focusedPlantableTiles.ResetTile();
        }
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Tile")
        {
            if(isDebugging)
                Debug.Log($"<b><color=blue>PlantableTileDetector</color>: <color=white>OnTriggerEnter</color> : <color=blue>{other.name}</color></b>");

            AddPlantableTile(other.GetComponent<PlantableTile>());

            SetFocusPlantableTile();
        }
    }

    private void OnTriggerExit(Collider other) 
    {
        if(other.gameObject.tag == "Tile")
        {
            if(isDebugging)
                Debug.Log($"<b><color=blue>PlantableTileDetector</color>: <color=white>OnTriggerExit</color> : <color=blue>{other.name}</color></b>");

            RemovePlantableTile(other.GetComponent<PlantableTile>());

            SetFocusPlantableTile();
        }
    }
}
