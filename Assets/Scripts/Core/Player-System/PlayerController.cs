﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;
using UnityEngine.AI;
using XInputDotNetPure;
using UnityEngine.UI;

struct LaunchData 
{
    public readonly Vector3 initialVelocity;
    public readonly float timeToTarget;

    public LaunchData (Vector3 initialVelocity, float timeToTarget)
    {
        this.initialVelocity = initialVelocity;
        this.timeToTarget = timeToTarget;
    }
    
}

public enum PlayerMovementStage
{
    idle,
    moving
}

public enum PlayerHandStage
{
    empty,
    holding,
    performing
}

public enum PlayerActionStage
{
    nothing,
    watering,
    usingItem,
    usingMarket,
    planting,
    harvesting,
    usingBat
}

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerController : MonoBehaviour
{
    private bool isDebugging = true;

    [SerializeField, BoxGroup("Player Stat Info")]
    private PlayerStat _playerStat;
    public PlayerStat playerStat { get { return _playerStat; } private set { _playerStat = value; }}

    [Space(10)]

    [SerializeField, BoxGroup("Player Stat Info")]
    private PlayerMovementStage _movementStage;
    public PlayerMovementStage movementStage { get { return _movementStage; } private set { _movementStage = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private PlayerHandStage _handStage;
    public PlayerHandStage handStage { get { return _handStage; } private set { _handStage = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private PlayerActionStage _actionStage;
    public PlayerActionStage actionStage { get { return _actionStage; } private set { _actionStage = value; }}

    [Space(10)]

    private float _speedModifier;
    public float speedModifier { get { return _speedModifier; } private set { _speedModifier = value; }}

    [SerializeField, BoxGroup("Holdable Setting")]
    private Holdable _holdingItem;
    public Holdable holdingItem { get { return _holdingItem; } private set { _holdingItem = value; }}
    [SerializeField, BoxGroup("Holdable Setting")]
    private Vector3 pickUpOffset;
    [SerializeField, BoxGroup("Holdable Setting")]
    private float pickUpArea = 0.5f;
    [SerializeField, BoxGroup("Holdable Setting")]
    private float usingBatArea = 2.0f;
    [SerializeField, BoxGroup("Holdable Setting")]
    private Transform holdingTrans;

    [SerializeField, BoxGroup("Throw Setting")]
    private float throwForce = 50f;

    [SerializeField, BoxGroup("Hit Force Setting")]
    private float hitForce = 2f;

    [SerializeField, BoxGroup("Planting Setting")]
    private PlantableTileDetector _plantableTileDetector;
    public PlantableTileDetector plantableTileDetector { get { return _plantableTileDetector; } private set { _plantableTileDetector = value; }}
    [SerializeField, BoxGroup("Tools Setting")]
    private Slider toolsSlider;
    [SerializeField, BoxGroup("Tools Setting")]
    private GameObject toolsSlider_obj;

    [SerializeField, BoxGroup("Animation Setting")]
    private Animator anim;
    [SerializeField, BoxGroup("Animation Setting")]
    private SpriteRenderer characterRenderer;

    [SerializeField, BoxGroup("Aimming Setting")]
    private Transform target;
    [SerializeField, BoxGroup("Aimming Setting")]
    private float h = 5;
    [SerializeField, BoxGroup("Aimming Setting")]
	private float gravity = -9.8f;
    [SerializeField, BoxGroup("Aimming Setting")]
    private float reticleSpeed = 2f;
    [SerializeField, BoxGroup("Aimming Setting")]
    private bool isAiming;
    [SerializeField, BoxGroup("Aimming Setting")]
    private float forceMultiplication = 2f;
    [SerializeField, BoxGroup("Joycontroller Settings")]
    private Vector2 vibrationValue = Vector2.one;
    [SerializeField, BoxGroup("Joycontroller Settings")]
    private float vibrationDuration = 0.2f;
    [SerializeField,  BoxGroup("Special Events")]
    private StatusIcon mark;
    private Coroutine joySelectionTimer;
    private Coroutine dizzy;
    private bool joySelectionValueAvailability;

    [SerializeField, BoxGroup("Respawn Info")]
    public Vector3 respawnPoint = Vector3.zero;
    [SerializeField, BoxGroup("Respawn Info")]
    private float respawnDelay = 3.0f;
    [SerializeField, BoxGroup("Respawn Info")]
    private float blinkingTime = 1.5f;
    private bool isBlinking = false;
    private Color originalColor;

    private AudioManager audioManager;
    [SerializeField, BoxGroup("Characters' SFX")]
    private AudioData sfx_Hurt;
    [SerializeField, BoxGroup("Characters' SFX")]
    private AudioData sfx_Happy;

    [SerializeField, BoxGroup("StunColor")]
    private Color stunColor = new Color(255f/255f, 126f/255f, 126f/255f);
    private bool isUsingStunColor;

    private Vector3 _lookDir;
    private Vector3 _dir;
    private Vector3 _dirReticle;
    private NavMeshAgent _agent;
    private bool _stopped;
    private bool _restricted;
    private Rigidbody rid;

    private Coroutine toolsLevelTimer;

    public delegate void IDArgs(PlayerID playerID);
    public IDArgs onPurchased;
    public IDArgs onPlanted;
    public IDArgs onHittedOther;
    public IDArgs onWatered;


    private void Awake() 
    {
        _agent = this.GetComponent<NavMeshAgent>();    
        _speedModifier = 1.0f;
        _restricted = false;
        isAiming = false;
        if(target != null)
            target.transform.position = transform.position;

        SetSpawnPoint();
        rid = this.GetComponent<Rigidbody>();
        //rid.isKinematic = true;
    }

    private void Start() 
    {
        audioManager = AudioManager.instance;
        if(PlayerManager.instance.IsPlayerStatContainPlayerID(playerStat.playerID) == true)
        {
            playerStat = PlayerManager.instance.GetPlayerStatByID(playerStat.playerID);
            movementStage = PlayerMovementStage.idle;
            handStage = PlayerHandStage.empty;
            actionStage = PlayerActionStage.nothing;
            target.gameObject.SetActive(false);
            originalColor = GetComponent<SpriteRenderer>().color;
            anim.runtimeAnimatorController = playerStat.character.character_animator;
            toolsSlider_obj.SetActive(false);
            PlayerManager.instance.AddPlayerController(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Update() 
    {
        // Reset input dir to zero.
        _dir = Vector3.zero;

        if(_restricted == false)
        {
            // Recieve movement input
            if(InputManager.GetAxisRaw("Horizontal", playerStat.playerID) >= 0.1f || InputManager.GetAxisRaw("Vertical", playerStat.playerID) >= 0.1f || InputManager.GetAxisRaw("Horizontal", playerStat.playerID) <= -0.1f || InputManager.GetAxisRaw("Vertical", playerStat.playerID) <= -0.1f)
            {
                if(isBlinking == true)
                    isBlinking = false;

                if(actionStage == PlayerActionStage.usingMarket && joySelectionValueAvailability == true)
                {
                    if(joySelectionTimer != null) StopCoroutine(joySelectionTimer);
                    joySelectionTimer = StartCoroutine(JoySelectionTimer());
                    int dir = (InputManager.GetAxisRaw("Horizontal", playerStat.playerID) > 0) ? 1 : -1;
                    UIMarket.instance.MoveCurser(playerStat.playerID, dir);
                }
                
                if(actionStage == PlayerActionStage.nothing)
                {
                    _stopped = false;

                    _dir = new Vector3(InputManager.GetAxisRaw("Horizontal", playerStat.playerID), 0, InputManager.GetAxisRaw("Vertical", playerStat.playerID));

                    _lookDir = _dir;

                    MoveTo(transform.position + _dir.normalized);

                    // Setting player movement stage
                    if(movementStage != PlayerMovementStage.moving)
                        movementStage = PlayerMovementStage.moving;
                }

                if(isAiming == true)
                {
                    SetAim();
                }
            }
            else
            {
                joySelectionValueAvailability = true;

                if(_stopped == false)
                {
                    ForceStop();
                }

                // Setting player movement stage
                if(movementStage != PlayerMovementStage.idle)
                    movementStage = PlayerMovementStage.idle;
            }

            // Recieve action input
            if(InputManager.GetButtonDown("Interact", playerStat.playerID) && handStage != PlayerHandStage.performing)
            {
                if(isDebugging)
                    Debug.Log($"<color=blue>PlayerController</color>: <color=green>Interact</color> button got pressed.");
                
                if(actionStage != PlayerActionStage.usingMarket)
                {
                    PerformEnterMarket();
                }
                else
                {
                    PerformBuyInMarket();
                }

                if(holdingItem == null)
                {
                    if(actionStage == PlayerActionStage.nothing)
                        PerformPickUp();
                }
                else
                {
                    if(isDebugging)
                        Debug.Log($"<color=blue>PlayerController</color>: <color=green>Performing</color> player is holding item.");
                    
                    if(holdingItem.holdableType == HoldableType.item)
                    {
                        handStage = PlayerHandStage.performing;
                        PerformUseItem();
                    }
                    else if(holdingItem.holdableType == HoldableType.tool)
                    {
                        handStage = PlayerHandStage.performing;
                        PerformUseTool();
                    }
                    else if(holdingItem.holdableType == HoldableType.seedBag)
                    {
                        handStage = PlayerHandStage.performing;
                        PerformPlanting();
                    }
                }
                
            }

            if(InputManager.GetButtonDown("Cancle", playerStat.playerID) && handStage != PlayerHandStage.performing)
            {
                if(actionStage == PlayerActionStage.usingMarket)
                {
                    PerformExitMarket();
                }
                else
                {
                    if(holdingItem != null && isAiming == false)
                    {
                        PerformDrop();
                    }

                    if(isAiming) isAiming = false;
                }
            }

            if(InputManager.GetButtonDown("Action", playerStat.playerID) && handStage != PlayerHandStage.performing)
            {
                if(isDebugging)
                    Debug.Log($"<color=blue>PlayerController</color>: <color=green>Action</color> button got pressed.");
            }

            if(InputManager.GetButtonDown("Throw", playerStat.playerID) && handStage != PlayerHandStage.performing)
            {
                if(isDebugging)
                    Debug.Log($"<color=blue>PlayerController</color>: <color=green>Throw</color> button got pressed.");

                if(holdingItem != null)
                {
                    //Use Throw function here
                    PerformThrow();
                }
            }
        }

        UpdateAnimator();
    }

    private void UpdateAnimator()
    {
        if(anim == null) return;

        anim.SetBool("isWalking", (movementStage == PlayerMovementStage.moving));
        anim.SetBool("isHolding", (holdingItem != null));

        if(characterRenderer == null) return;

        if(_lookDir.x != 0)
        {
            Vector3 localScale = new Vector3(Mathf.Abs(characterRenderer.transform.localScale.x) * -_lookDir.x/Mathf.Abs(_lookDir.x), characterRenderer.transform.localScale.y, characterRenderer.transform.localScale.z);
            characterRenderer.transform.localScale = localScale;
        }
    }

    private void PerformUseItem()
    {
        Items item = holdingItem.GetComponent<Items>();
        handStage = PlayerHandStage.holding;
        if(item.itemType == ItemType.Fertilizer && plantableTileDetector.IsOnPlantableTile() == true
        && plantableTileDetector.focusedPlantableTiles.GetTileState() != PlantableTile.PlantableTileState.empty
        && plantableTileDetector.focusedPlantableTiles.GetTileState() != PlantableTile.PlantableTileState.harvestable)
        {
            FertilizerController fertilizerController = holdingItem.GetComponent<FertilizerController>();
            fertilizerController.FertilizerCount();
            plantableTileDetector.AttemptFertilizeCrop(item.itemData.itemID);
        }
        else if(item.itemType == ItemType.MrMole)
        {
            if(!isAiming)
            { 
                isAiming = true;
                actionStage = PlayerActionStage.usingItem;
                target.gameObject.SetActive(true);
            }
            else
            {
                target.gameObject.SetActive(false);
                PerformThrow();
                holdingItem.GetComponent<MrMoleController>().UseItem(target.position - holdingItem.transform.position);
                AudioManager.instance.Play(sfx_Happy);
            }
        }
        else if(item.itemType == ItemType.Cloud)
        {
            if(!isAiming)
            { 
                isAiming = true;
                actionStage = PlayerActionStage.usingItem;
                target.gameObject.SetActive(true);
            }
            else
            {
                target.gameObject.SetActive(false);
                PerformThrow();
                holdingItem.GetComponent<CloudController>().UseItem(target.position - holdingItem.transform.position);
                AudioManager.instance.Play(sfx_Happy);
            }
        }
    }

    private void PerformUseTool()
    {
        Tools tool = holdingItem.GetComponent<Tools>();

        if(tool.toolType == ToolType.waterBucket)
        {
            PerformRefillWater(tool);
            // if(toolsLevelTimer != null) StopCoroutine(toolsLevelTimer);
            //toolsLevelTimer = StartCoroutine(ShowToolsLevel(tool.GetComponent<BucketController>().waterLevel));
        }

        if(tool.toolType == ToolType.bat)
        {
            PerformUseBat();
        }

        if(tool.toolType != ToolType.bat &&  plantableTileDetector.IsOnPlantableTile() == true && tool.UseTool() == true)
        {
            if(tool.toolType == ToolType.waterBucket)
            {
                if(anim != null)
                    anim.SetTrigger("Watering");

                // if(toolsLevelTimer != null) StopCoroutine(toolsLevelTimer);
                //toolsLevelTimer = StartCoroutine(ShowToolsLevel(tool.GetComponent<BucketController>().DecreaseWaterLevel()));

                tool.GetComponent<BucketController>().DecreaseWaterLevel();
                
                if(plantableTileDetector.focusedPlantableTiles.GetTileState() == PlantableTile.PlantableTileState.planted)
                {
                    actionStage = PlayerActionStage.watering;
                
                    //Karma
                    KarmaStats tileKarma = plantableTileDetector.focusedPlantableTiles.transform.GetComponent<KarmaStats>();
                
                    if(tileKarma.isWaterOnce == false)
                    {
                        tileKarma.isWaterOnce = true;
                        tileKarma.SetPlayerWater(playerStat.playerID);
                    }
                    else if(tileKarma.isWaterTwice == false)
                    {
                        tileKarma.isWaterTwice = true;
                        tileKarma.SetPlayerWater(playerStat.playerID);
                    }
                
                    if(tileKarma.player_Plant == playerStat.playerID)
                    {
                        playerStat.GainKarmaPoint(0.5f);
                    }

                    if(onWatered != null)
                        onWatered.Invoke(playerStat.playerID);
                }
                _restricted = true;
                ForceStop();
            }
            else if(tool.toolType == ToolType.scythe)
            {
                actionStage = PlayerActionStage.harvesting;
                
                if(anim != null)
                    anim.SetTrigger("Harvesting");

                //Karma
                KarmaStats tileKarma = plantableTileDetector.focusedPlantableTiles.transform.GetComponent<KarmaStats>();
                tileKarma.SetPlayerHarvest(playerStat.playerID);
                if(plantableTileDetector.focusedPlantableTiles.GetTileState() == PlantableTile.PlantableTileState.harvestable)
                {
                    audioManager.Play(audioManager.playerAudioList.sfx_harvesting);
                    
                    if(tileKarma.player_Plant == playerStat.playerID)
                    {
                        playerStat.GainKarmaPoint(0.25f);
                    }
                    else
                    {
                        playerStat.LossKarmaPoint(2.5f);
                    }
                }
                

                _restricted = true;
                ForceStop();
            }

            StartCoroutine(SetVibration(vibrationValue, vibrationDuration));
        }

        handStage = PlayerHandStage.holding;
        actionStage = PlayerActionStage.nothing;
    }

    private void PerformPlanting()
    {
        SeedBagController seedbagController = holdingItem.GetComponent<SeedBagController>();
        if(plantableTileDetector.AttemptPlantCrop(seedbagController.cropData, playerStat.playerID) == true)
        {
            actionStage = PlayerActionStage.planting;

            //Karma
            KarmaStats tileKarma = plantableTileDetector.focusedPlantableTiles.transform.GetComponent<KarmaStats>();
            tileKarma.SetPlayerBuy(seedbagController.playerBuyID);
            tileKarma.SetPlayerPlant(playerStat.playerID);

            if(tileKarma.player_Buy == tileKarma.player_Plant)
            {
                playerStat.GainKarmaPoint(0.5f);
            }
            else
            {
                playerStat.LossKarmaPoint(2.5f);
            }
            

            // Check if there is seed left.
            if(seedbagController.UseSeed())
            {
                handStage = PlayerHandStage.holding;
            }
            else
            {
                holdingItem = null;
                handStage = PlayerHandStage.empty;
            }

            StartCoroutine(SetVibration(vibrationValue, vibrationDuration));
            actionStage = PlayerActionStage.nothing;

            audioManager.Play(audioManager.playerAudioList.sfx_planting);

            if(onPlanted != null)
                onPlanted.Invoke(playerStat.playerID);
        }
        else
        {
            handStage = PlayerHandStage.holding;
        }

    }

    public void HarvestingAnimationTrigger()
    {   
        if(holdingItem != null)
        {
            Tools tool = holdingItem.GetComponent<Tools>();
            if(tool.toolType == ToolType.scythe)
            {
                plantableTileDetector.AttemptHarvesting(playerStat.playerID);
            }
        }   
    }

    public void WateringAnimationTrigger()
    {
        plantableTileDetector.AttemptWatering();
        audioManager.Play(audioManager.playerAudioList.sfx_watering);
    }

    private void PerformPickUp()
    {
        RaycastHit[] hit = PerformRaycastCheck();
        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].transform.gameObject.layer == LayerMask.NameToLayer("Holdable"))
            {
                if(hit[i].transform.gameObject.GetComponent<Holdable>().CanPickUp() == true)
                {
                    holdingItem = hit[i].transform.GetComponent<Holdable>();
                    if(holdingItem != null && holdingItem.isHolding == true)
                    {
                        holdingItem = null;
                        break;
                    }
                    if(anim != null)
                        anim.SetTrigger("Pickup");
                
                    _restricted = true;
                    ForceStop();

                    break;
                } 
            }
        }


        if(holdingItem != null)
        {
            handStage = PlayerHandStage.holding;
        }
    }

    public void PerformPickUp(Transform target)
    {
        holdingItem = target.GetComponent<Holdable>();

        if(anim != null)
            anim.SetTrigger("Pickup");

        _restricted = true;
        ForceStop();

        if(holdingItem != null)
            handStage = PlayerHandStage.holding;
    }

    public void PickupAnimationTrigger()
    {
        if(holdingItem != null)
        {
            if(holdingItem.isHolding == true)
            {
                holdingItem = null;
                return;
            }
            holdingItem.PickUp(holdingTrans, playerStat.playerID);
            audioManager.Play(audioManager.playerAudioList.sfx_pickup);
        }
    }

    public void DoneAnimationState()
    {
        _restricted = false;
    }

    private void PerformRefillWater(Tools tool)
    {
        RaycastHit[] hit = PerformRaycastCheck();
        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].transform.gameObject.tag == "Well")
            {
                tool.ReloadTool();
                audioManager.Play(audioManager.playerAudioList.sfx_fillBucket);
                break;
            }
        }
    }

    private RaycastHit[] PerformRaycastCheck()
    {
        return Physics.SphereCastAll(this.transform.position, pickUpArea, pickUpOffset);
    }

    private RaycastHit[] PerformBatRaycastCheck()
    {
        return Physics.SphereCastAll(this.transform.position, usingBatArea, pickUpOffset);
    }

    private void PerformDrop()
    {
        holdingItem.PutDown();
        holdingItem = null;
        audioManager.Play(audioManager.playerAudioList.sfx_droping);

        handStage = PlayerHandStage.empty;
    }

    public void ThrowAnimationTrigger()
    {
        if(holdingItem != null)
        {
            if(isAiming == false)
            {
                holdingItem.Throw(_lookDir + Vector3.up, throwForce);
            }
            else
            {
                holdingItem.Throw(CalculateLaunchData().initialVelocity * forceMultiplication);
                ResetAim();
            }
            holdingItem = null;

            audioManager.Play(audioManager.playerAudioList.sfx_throwing);

            handStage = PlayerHandStage.empty;
        }
    }

    private void PerformThrow()
    {
        if(anim != null)
            anim.SetTrigger("Throw");

        _restricted = true;
        ForceStop();
    }

    private void PerformUseBat()
    {
        Tools tool = holdingItem.GetComponent<Tools>();

        if(tool.UseTool())
        {
            actionStage = PlayerActionStage.usingBat;
            if(anim != null)
                anim.SetTrigger("Harvesting");

            RaycastHit[] hit = PerformBatRaycastCheck();

            for (int i = 0; i < hit.Length; i++)
            {
                if ((hit[i].transform.gameObject.tag == "Player") && hit[i].transform.name != this.transform.name)
                {
                    hit[i].transform.GetComponent<PlayerController>().Knocked(this.transform.position, 1.5f);
                    tool.ReloadTool();
                    if(tool.canOnlyUsedOnce == true)
                    {                
                        PerformDrop();
                    }

                    //Karma
                    playerStat.LossKarmaPoint(2.5f);

                    if(onHittedOther != null)
                        onHittedOther.Invoke(playerStat.playerID);
                    break;
                }
            }

            actionStage = PlayerActionStage.nothing;

            _restricted = true;
            ForceStop();    
        }    
    }

    private void PerformEnterMarket()
    {
        if(holdingItem != null)
            return;

        RaycastHit[] hit = PerformRaycastCheck();
        for (int i = 0; i < hit.Length; i++)
        {
            if (hit[i].transform.gameObject.tag == "Market")
            {
                actionStage = PlayerActionStage.usingMarket;
                UIMarket.instance.PlayerEnterMarket(playerStat.playerID);
                audioManager.Play(audioManager.playerAudioList.sfx_openshop);
                break;
            }
        }
    }

    public void PerformExitMarket()
    {
        actionStage = PlayerActionStage.nothing;
        UIMarket.instance.PlayerExitMarket(playerStat.playerID);
    }

    private void PerformBuyInMarket()
    {
        if(UIMarket.instance.PlayerAttemptBuy(playerStat.playerID) == true)
        {
            audioManager.Play(audioManager.playerAudioList.sfx_buying);

            if(onPurchased != null)
                onPurchased.Invoke(playerStat.playerID);
        }
        
    }

    public void ResetPlayerStat()
    {
        playerStat.Reset();
    }

    // Move character to position
    private void MoveTo(Vector3 pos)
    {
        if(_agent == null)
            return;
        // Apply character movement to agent
        _agent.speed = playerStat.movementSpeed * _speedModifier;

        _agent.isStopped = false;

        _agent.SetDestination(pos);
    }   

    public void Knocked(Vector3 playerPosition, float duration) // the parameter is the position of the player who use the bat
    {
        StartCoroutine(GotKnocked(playerPosition, duration));
    }

    IEnumerator GotKnocked(Vector3 playerPosition, float duration)
    {
        //Drop item
        if(holdingItem != null)
        {
            PerformDrop();
        }

        //Knock yourself into given direction, using this player position minus the one who use the bat to get percise direction
        Vector3 hitDirection = this.transform.position - playerPosition;
        hitDirection.y = 0;
        rid.isKinematic = false;
        _agent.enabled = false;
        yield return new WaitForFixedUpdate();
        rid.AddForce(hitDirection.normalized * hitForce, ForceMode.Impulse);

        AudioManager.instance.Play(sfx_Hurt);

        //Show Excalamtion mark
        mark.Show(mark.iconList.stun, duration);

        StartCoroutine(SetVibration(vibrationValue * 2f, vibrationDuration * 2f));

        //Unable to do anything for (duration) sec
        if(dizzy != null) StopCoroutine(dizzy);
        dizzy = StartCoroutine(Dizzy(duration));

        StartCoroutine(BlinkingCharacter(duration));
    }

    // Force stop character
    private void ForceStop()
    {
        _stopped = true;
        if(_agent != null)
        {
            _agent.velocity = Vector3.zero;
            MoveTo(transform.position);

            _agent.isStopped = true;
        }
    }

    IEnumerator JoySelectionTimer()
    {
        joySelectionValueAvailability = false;
        float duration = 1f;
        while (duration > 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }
        joySelectionValueAvailability = true;
    }

    IEnumerator Dizzy(float duration)
    {
        _restricted = true;

        while (duration > 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }
        _restricted = false;
        _agent.enabled = true;

        UpdateIcon();
    }

    LaunchData CalculateLaunchData() 
	{
		float displacementY = target.position.y - holdingItem.transform.position.y;
		Vector3 displacementXZ = new Vector3 (target.position.x - holdingItem.transform.position.x, 0, target.position.z - holdingItem.transform.position.z);
		float time = Mathf.Sqrt(-2*h/gravity) + Mathf.Sqrt(2*(displacementY - h)/gravity);
		Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
		Vector3 velocityXZ = displacementXZ / time;

		return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
	}

    private void SetAim()
    {
        _dirReticle = new Vector3(InputManager.GetAxisRaw("Horizontal", playerStat.playerID), 0, InputManager.GetAxisRaw("Vertical", playerStat.playerID)); 

        target.transform.position += _dirReticle.normalized * reticleSpeed * Time.deltaTime;

        if(_stopped == false)
        {
            _stopped = true;
            ForceStop();
        }

        if(movementStage != PlayerMovementStage.idle)
            movementStage = PlayerMovementStage.idle;
    }

    private void ResetAim()
    {
        isAiming = false;
        actionStage = PlayerActionStage.nothing;

        if(target != null)
            target.transform.position = transform.position;
    }

    IEnumerator SetVibration(Vector2 v_value, float duration)
    {
        if(playerStat.IsJoyEnabled == true)
        {
            GamePad.SetVibration(playerStat.playerIndex, v_value.x, v_value.y);
            yield return new WaitForSeconds(duration);
            GamePad.SetVibration(playerStat.playerIndex, 0, 0);
        }
    }

    IEnumerator BlinkingCharacter(float duration)
    {
        isBlinking = true;

        if(GetComponent<SpriteRenderer>().material != null)
        {
            Material material = GetComponent<SpriteRenderer>().material;
            material.SetColor("_Color", Color.white);

            //How long player appear as white;
            yield return new WaitForSeconds(0.25f);
        }
        
        while(duration > 0)
        {
            duration -= 0.25f;

            if(isBlinking == false)
            {
                break;
            }
            else if(isUsingStunColor == false)
            {
                //Transparent
                //characterRenderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a * 0.25f);
                characterRenderer.color = new Color(stunColor.r, stunColor.g, stunColor.b, originalColor.a);
                isUsingStunColor = true;
            }   
            else
            {
                characterRenderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a);
                isUsingStunColor = false;
            }
                
            
            //How frequent the player blink
            yield return new WaitForSeconds(0.25f);
        }

        characterRenderer.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a);
        isUsingStunColor = false;
    }

    private void SetSpawnPoint()
    {
       if(respawnPoint == Vector3.zero)
            respawnPoint = transform.position;
    }

    [Button]
    private void Respawn()
    {
        this.gameObject.SetActive(false);

        SpawnManager.instance.Spawn(this.gameObject,respawnPoint, respawnDelay);
    }  

    public void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.CompareTag("FallPlain"))
        {
            Respawn();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("FallSoundTrigger"))
        {
            audioManager.Play(audioManager.playerAudioList.sfx_playerFall);
        }
    }

    public void Blinking()
    {
        StartCoroutine(BlinkingCharacter(blinkingTime));
    }

    public void UpdateIcon()
    {
        if(KarmaManager.instance.KarmaPointToLevel(playerStat) <= -1)
        {
            mark.Show(mark.iconList.devil);
        }
        else if(KarmaManager.instance.KarmaPointToLevel(playerStat) >= 1)
        {
            mark.Show(mark.iconList.halo);
        }
        else
        {
            mark.ClearShow();
        }
    }

    IEnumerator ShowToolsLevel(int waterLevel)
    {
        toolsSlider_obj.SetActive(true);
        toolsSlider.value = waterLevel;
        yield return new WaitForSeconds(1f);
        toolsSlider_obj.SetActive(false);
    }

    public void ClearStatus()
    {
        _restricted = false;
        _agent.enabled = true;

        handStage = PlayerHandStage.empty;
        movementStage = PlayerMovementStage.idle;
        actionStage = PlayerActionStage.nothing;

        if(playerStat.IsJoyEnabled == true)
            GamePad.SetVibration(playerStat.playerIndex, 0, 0);

        UpdateIcon();
    }
}
