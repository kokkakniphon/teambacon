﻿using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Luminosity.IO;
using XInputDotNetPure;

[System.Serializable]
public class Currency
{
    public int softCurrency;
    public int hardCurrency;
}

[System.Serializable]
public class KarmaPoint
{
    public float karmaPoint;
    public float maxKarmaPoint = 25.0f;
    public float minKarmaPoint = -25.0f;
}

[System.Serializable]
public class PlayerStat
{
    [SerializeField, BoxGroup("Player Stat Info")]
    private Color _playerColor = Color.white;
    public Color playerColor { get => _playerColor; private set => _playerColor = value; }
    [SerializeField, BoxGroup("Player Stat Info")]
    private CharacterSprites _character;
    public CharacterSprites character { get { return _character; } private set { _character = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private PlayerID _playerID;
    public PlayerID playerID { get { return _playerID; } private set { _playerID = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private PlayerIndex _playerIndex;
    public PlayerIndex playerIndex { get { return _playerIndex; } private set { _playerIndex = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private bool isJoyEnabled = false;
    public bool IsJoyEnabled { get => isJoyEnabled; set => isJoyEnabled = value; }
    [SerializeField, BoxGroup("Player Stat Info")]
    private string _playerName = "Default";
    public string playerName { get { return _playerName; } private set { _playerName = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private float _movementSpeed = 1f;
    public float movementSpeed { get { return _movementSpeed; } private set {_movementSpeed = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private KarmaPoint _playerKarma = new KarmaPoint();
    public KarmaPoint playerKarma { get { return _playerKarma; } private set { _playerKarma = value; }}
    [SerializeField, BoxGroup("Player Stat Info")]
    private Currency _playerCurrency;
    public Currency playerCurrency { get { return _playerCurrency; } private set { _playerCurrency = value; }}
    public int totalPlayerAsset { get { return playerCurrency.softCurrency + playerCurrency.hardCurrency; }}

    [SerializeField, BoxGroup("Player Stat Info")]
    private int _playeRank;
    public int playerRank { get { return _playeRank; } private set { _playeRank = value; }}

    public void Reset() 
    { 
        playerCurrency = new Currency();    
        playerKarma = new KarmaPoint();
    }

    public PlayerStat(PlayerID value_playerID, string value_playerName, Color playerColor)
    {
        SetPlayerID(value_playerID);
        SetPlayerName(value_playerName);
        _playerColor = playerColor;
    }
    public void SetCharacter(CharacterSprites value_sprite) { character = value_sprite; }
    public void SetPlayerID(PlayerID value_playerID) { playerID = value_playerID; }
    public void SetPlayerName(string value_string) { playerName = value_string; }
    public void SetSoftCurrency(int value_int) { playerCurrency.softCurrency = value_int; }
    public void SetHardCurrency(int value_int) { playerCurrency.hardCurrency = value_int; }
    public void GainSoftCurrency(int value_int) { playerCurrency.softCurrency += value_int; }
    public void LossSoftCurrency(int value_int) { playerCurrency.softCurrency -= value_int; }
    public void GainHardCurrency(int value_int) { playerCurrency.softCurrency += value_int; }
    public void SetKarmaPoint(float value_float) 
    {  
        if(value_float > playerKarma.maxKarmaPoint)
            playerKarma.karmaPoint = playerKarma.maxKarmaPoint;
        else if(value_float < playerKarma.minKarmaPoint)
            playerKarma.karmaPoint = playerKarma.minKarmaPoint;
        else
            playerKarma.karmaPoint = value_float; 
    }
    public void GainKarmaPoint(float value_float) 
    { 
        playerKarma.karmaPoint += value_float;

        if(playerKarma.karmaPoint > playerKarma.maxKarmaPoint)
            playerKarma.karmaPoint = playerKarma.maxKarmaPoint;

        PlayerManager.instance.GetPlayerControllerByID(playerID).UpdateIcon();
        UIPlayerManager.instance.UpdateUI();
    }
    public void LossKarmaPoint(float value_float) 
    { 
        playerKarma.karmaPoint -= value_float;

        if(playerKarma.karmaPoint  < playerKarma.minKarmaPoint)
            playerKarma.karmaPoint = playerKarma.minKarmaPoint;

        PlayerManager.instance.GetPlayerControllerByID(playerID).UpdateIcon();
        UIPlayerManager.instance.UpdateUI();
    }
    public void SetPlayerIndex(PlayerIndex index) { playerIndex = index;}

    public void SetPlayerRank(int value)
    {
        playerRank = value;
    }
}

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance;
    
    [SerializeField, BoxGroup("General Settings")]
    private List<PlayerStat> _allPlayerStat = new List<PlayerStat>();
    public List<PlayerStat> allPlayerStat { get => _allPlayerStat; private set { _allPlayerStat = value; }}

    [SerializeField, BoxGroup("General Settings")]
    private List<PlayerController> _allPlayerController = new List<PlayerController>();
    public List<PlayerController> allPlayerController { get => _allPlayerController; private set { _allPlayerController = value; }}

    private void Awake() 
    {
        if(instance != null)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    public void AddPlayerStat(PlayerStat value_object)
    {
        allPlayerStat.Add(value_object);
    }

    public void AddPlayerController(PlayerController value_object)
    {
        allPlayerController.Add(value_object);
    }

    public void RemovePlayerStat(PlayerID playerID)
    {
        bool founded = false;
        for (int i = 0; i < allPlayerStat.Count; i++)
        {
            if(allPlayerStat[i].playerID == playerID)
            {
                allPlayerStat.RemoveAt(i);
                founded = true;
            }
        }

        if(founded == false)
            Debug.LogError($"<b><color=red>PlayerManager</color>: Can't find player stat with player id of: <color=red>{playerID}</color>.</b>");
    }

    public void ClearPlayerStat()
    {
        allPlayerStat.Clear();
    }
    public void ClearPlayerController()
    {
        allPlayerController.Clear();
    }

    public bool IsPlayerStatContainPlayerID(PlayerID playerID)
    {
        for (int i = 0; i < allPlayerStat.Count; i++)
        {
            if(allPlayerStat[i].playerID == playerID)
                return true;
        }

        return false;
    }

    public PlayerStat GetPlayerStatByID(PlayerID playerID)
    {
        for (int i = 0; i < allPlayerStat.Count; i++)
        {
            if(allPlayerStat[i].playerID == playerID)
                return allPlayerStat[i];
        }

        Debug.LogError($"<b><color=red>PlayerManager</color>: Can't find player stat with player id of: <color=red>{playerID}</color>.</b>");

        return null;
    }

    public PlayerController GetPlayerControllerByID(PlayerID playerID)
    {
        for (int i = 0; i < allPlayerController.Count; i++)
        {
            if(allPlayerController[i].playerStat.playerID == playerID)
                return allPlayerController[i];
        }

        Debug.LogError($"<b><color=red>PlayerManager</color>: Can't find player controller with player id of: <color=red>{playerID}</color>.</b>");

        return null;
    }
}
