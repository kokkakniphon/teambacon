﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ProjectileCalculation : MonoBehaviour
{
	public Rigidbody ball;
	public Transform target;

	public float h = 25;
	public float gravity = -18;

	public bool debugPath;

	void Start() {
		ball.useGravity = false;
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Launch ();
		}

		if (debugPath) {
			DrawPath ();
		}
	}

	void Launch() {
		Physics.gravity = Vector3.up * gravity;
		ball.useGravity = true;
		ball.velocity = CalculateLaunchData ().initialVelocity;
	}

	LaunchData CalculateLaunchData() {
		float displacementY = target.position.y - ball.position.y;
		Vector3 displacementXZ = new Vector3 (target.position.x - ball.position.x, 0, target.position.z - ball.position.z);
		float time = Mathf.Sqrt(-2*h/gravity) + Mathf.Sqrt(2*(displacementY - h)/gravity);
		Vector3 velocityY = Vector3.up * Mathf.Sqrt (-2 * gravity * h);
		Vector3 velocityXZ = displacementXZ / time;

		return new LaunchData(velocityXZ + velocityY * -Mathf.Sign(gravity), time);
	}

	void DrawPath() {
		LaunchData launchData = CalculateLaunchData ();
		Vector3 previousDrawPoint = ball.position;

		int resolution = 30;
		for (int i = 1; i <= resolution; i++) {
			float simulationTime = i / (float)resolution * launchData.timeToTarget;
			Vector3 displacement = launchData.initialVelocity * simulationTime + Vector3.up *gravity * simulationTime * simulationTime / 2f;
			Vector3 drawPoint = ball.position + displacement;
			Debug.DrawLine (previousDrawPoint, drawPoint, Color.green);
			previousDrawPoint = drawPoint;
		}
	}

	struct LaunchData {
		public readonly Vector3 initialVelocity;
		public readonly float timeToTarget;

		public LaunchData (Vector3 initialVelocity, float timeToTarget)
		{
			this.initialVelocity = initialVelocity;
			this.timeToTarget = timeToTarget;
		}
		
	}


    /*public float initialVelocity;
    public float distanceToTarget;
    public float playerHeight;

    public float CalculateVelocity()
    {
        /*float constant = (0.5f * Physics.gravity.magnitude) / Mathf.Pow(Mathf.Sin(Mathf.Deg2Rad * 45),2);
        initialVelocity = Mathf.Sqrt((constant * mass * Mathf.Pow(distanceToTarget,2)) / (playerHeight + distanceToTarget));

        float constant = 2 * (-playerHeight - distanceToTarget * (Mathf.Tan(Mathf.Deg2Rad * 45)) * Mathf.Cos(Mathf.Deg2Rad * 45));
        initialVelocity = (Mathf.Pow(distanceToTarget,2) * -Physics.gravity.magnitude * rb.mass) / constant;

        Debug.Log("Initial Velocity: " + initialVelocity);
        Debug.Log("Constance:" + constant);

        return initialVelocity;
    }*/
}
