﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class UIPlayerManager : MonoBehaviour
{
    [System.Serializable]
    public class Rank_Sprites
    {
        [SerializeField, BoxGroup("Ranking"), ShowAssetPreview]
        public Sprite rankOne;
        [SerializeField, BoxGroup("Ranking"), ShowAssetPreview]
        public Sprite rankTwo;
        [SerializeField, BoxGroup("Ranking"), ShowAssetPreview]
        public Sprite rankThree;
        [SerializeField, BoxGroup("Ranking"), ShowAssetPreview]
        public Sprite rankFour;
    }

    public Rank_Sprites rank_Sprites = new Rank_Sprites();

    public static UIPlayerManager instance;

    [SerializeField, BoxGroup("General Settings"), ReorderableList]
    private List<UIPlayerInfo> playerInfos = new List<UIPlayerInfo>();

    private void Awake() 
    {
        if(instance != null)
            Destroy(instance.gameObject);

        instance = this;
    }

    private void Start() 
    {
        StartCoroutine(SetupData());
    }

    IEnumerator SetupData()
    {
        yield return new WaitUntil(() => (PlayerManager.instance.allPlayerStat.Count > 0));
        Setup();
    }

    public void Setup()
    {
        for (int i = 0; i < playerInfos.Count; i++)
        {
            playerInfos[i].Setup();
        }
    }

    public void UpdateUI()
    { 
        
        RankingManager.instance.SortScore();
        RankingManager.instance.AssignRank(); 
        
        for (int i = 0; i < playerInfos.Count; i++)
        {
            playerInfos[i].UpdateInfo();
        }
    }
}
