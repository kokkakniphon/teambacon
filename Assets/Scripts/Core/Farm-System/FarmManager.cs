﻿using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
public class FarmManager : MonoBehaviour
{
    [SerializeField, BoxGroup("Farm Manager"), ReorderableList]
    private List<PlantableTile> _allTiles = new List<PlantableTile>();
    public List<PlantableTile> allTiles { get { return _allTiles; } private set { _allTiles = value; }}
    [SerializeField, BoxGroup("Tile sprites"), ShowAssetPreview]
    private Sprite _dryTile;
    public Sprite dryTile { get { return _dryTile; } private set { _dryTile = value; }}
    [SerializeField, BoxGroup("Tile sprites"), ShowAssetPreview]
    private Sprite _wateredTile;
    public Sprite wateredTile { get { return _wateredTile; } private set { _wateredTile = value; }}
   
    //Methods
    public void createTile(){
        PlantableTile tile = new PlantableTile();
       // _allTiles.Add(tile);
    }

    public void removeTile(Vector2 v){
        //_allTiles.Remove(PlantableTile);
    }

    public void cleanAllTiles(){
        foreach(PlantableTile i in _allTiles){
            //reset value
        }
    }

    public void getPlantedTiles(){}
}
