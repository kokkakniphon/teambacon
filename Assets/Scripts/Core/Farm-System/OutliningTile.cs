﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class OutliningTile : MonoBehaviour
{
    [SerializeField]
    public SpriteRenderer outlinedTile;
    void Start()
    {
        this.outlinedTile = this.GetComponent<SpriteRenderer>();
        outlinedTile.enabled = false;
    }

    public void UpdateOutline(bool outline) {
        if (outline == true) outlinedTile.enabled = true;
        else { outlinedTile.enabled = false; }
        
    }
}
