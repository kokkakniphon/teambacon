﻿using NaughtyAttributes;
using UnityEngine;

public class CropController : Holdable
{
    [SerializeField, BoxGroup("General Info")]
    private CropData _cropData;
    public CropData cropData { get { return _cropData; } private set { _cropData = value; }}

    [SerializeField, BoxGroup("General Info")]
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        SetHoldableType(HoldableType.crop);
    }

    private void Start() 
    {
        SetupSeedBag();    
    }

    public void Selled()
    {
        Destroy(this.gameObject);
    }

    public void SetupCrop(string cropID)
    {
        cropData = GM.instance.cropDB.GetCropDataByID(cropID);

        if(cropData == null) return;

        spriteRenderer.sprite = cropData.cropIcon;
    }

    public void SetupSeedBag()
    {
        if(cropData == null) return;

        spriteRenderer.sprite = cropData.cropIcon;
    }

}
