﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class waterTile : MonoBehaviour
{
    public Sprite[] waterStages;
    private SpriteRenderer _SR;

    void Awake()
    {
        _SR = this.GetComponent<SpriteRenderer>();
        _SR.color = new Color (_SR.color.a, _SR.color.g, _SR.color.b, 0f);
        _SR.sprite = waterStages[0];
        //set to water 100% and turn it off
    }

    public void UpdateWaterStages(float humidityValue)
    {
        _SR.color = new Color (_SR.color.a, _SR.color.g, _SR.color.b, 1f);
        if (humidityValue > 0.75){_SR.sprite = waterStages[0];}
        else if (humidityValue > 0.50) {_SR.sprite = waterStages[1];}
        else if (humidityValue > 0.25) {_SR.sprite = waterStages[2];}
        else if (humidityValue > 0) {_SR.sprite = waterStages[3];}
        else
        {
            _SR.color = new Color (_SR.color.a, _SR.color.g, _SR.color.b, 0f);
        }
    }

    [Button]
    public void DryTile()
    {
        _SR.enabled = false;
        _SR.color = new Color (_SR.color.a, _SR.color.g, _SR.color.b, 0f);
    }
}

