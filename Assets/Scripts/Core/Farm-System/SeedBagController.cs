﻿using NaughtyAttributes;
using UnityEngine;
using Luminosity.IO;

public class SeedBagController : Holdable
{
    [SerializeField, BoxGroup("General Info")]
    private CropData _cropData;
    public CropData cropData { get { return _cropData; } private set { _cropData = value; }}

    [SerializeField, BoxGroup("General Info")]
    private SpriteRenderer spriteRenderer;

    [SerializeField, BoxGroup("General Info")]
    private int _seedAmount = 5;
    public int seedAmount {get { return _seedAmount; } private set { _seedAmount = value; }}

    //Karma
    [SerializeField, BoxGroup("Karma Info"), ReadOnly]
    public PlayerID playerBuyID;

    [SerializeField, BoxGroup("Respawn")]
    private float spawnTimer = 2.5f;

    private void Awake()
    {
        SetHoldableType(HoldableType.seedBag);
    }

    private void Start() 
    {
        SetupSeedBag();    
    }

    public void SetupSeedBag(string cropID)
    {
        cropData = GM.instance.cropDB.GetCropDataByID(cropID);

        if(cropData == null) return;

        spriteRenderer.sprite = cropData.seedBagImage;
    }

    public void SetupSeedBag()
    {
        if(cropData == null) return;

        spriteRenderer.sprite = cropData.seedBagImage;
    }

    public bool UseSeed()
    {
        seedAmount--;
        if(seedAmount > 0)
        {
            return true;
        }
        else
        {
            OutOfSeed();
            return false;  
        }
    }

    private void OutOfSeed()
    {
        Destroy(this.gameObject);
    }

    public void SetPlayerBuyID(PlayerID input)
    {
        playerBuyID = input;
    }

    private void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.CompareTag("FallPlain"))
        {
            SpawnManager.instance.Spawn(this.gameObject, PlayerManager.instance.GetPlayerControllerByID(playerBuyID).respawnPoint, spawnTimer);
        }
            
    }
}
