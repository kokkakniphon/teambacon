﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Glowing : MonoBehaviour
{
    public Renderer glowMaterial;
    void Start()
    {
        glowMaterial = this.GetComponent<Renderer>();
    }

    [Button]
    public void GlowOn() {glowMaterial.material.SetInt("_showEffect", 1);}
    
    [Button]
    public void GlowOff() {glowMaterial.material.SetInt("_showEffect", 0);}


}
