﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using NaughtyAttributes;
using Luminosity.IO;

public class MetaOnboarding : MonoBehaviour
{
    private bool isDebugging = true;
    private bool nextStepReady = false;
    public static bool isOnboarding = false;

    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_Setup;

    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_Intro;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_UseMarket;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_Planting;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_Watering;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_Harvesting;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_Selling;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_UseBat;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_KarmaIntro_PartOne;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_KarmaIntro_PartTwo;
    [BoxGroup("Meta stops invokes"), SerializeField]
    private UnityEvent meta_CompletedTutorial;

    [BoxGroup("Custom Traggers")]
    private List<PlayerController> allPlayers = new List<PlayerController>();

    private bool isHarvestable;

    private void Start() 
    {
        StartCoroutine(OnboardingCheck());
    }

    IEnumerator OnboardingCheck()
    {
        isOnboarding = true;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Waiting for data being loaded...</b>");

        yield return new WaitUntil(() => GM.instance != null);
        yield return new WaitUntil(() => PlayerManager.instance != null);
        yield return new WaitUntil(() => PlayerManager.instance.allPlayerController.Count != 0);

        //Setup everything before start.
        meta_Setup.Invoke();

        #region Introduction

        nextStepReady = false;
        
        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Wait for introduction to be done.</b>");

        meta_Intro.Invoke();

        //Wait until introduction is done.
        yield return new WaitForSeconds(4f);

        #endregion

        #region UseMarket

        nextStepReady = false;
        
        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Wait until all player buy seed from market.</b>");

        meta_UseMarket.Invoke();
        SetupUseMarket();

        //Wait until done with using market.
        yield return new WaitUntil(() => nextStepReady);

        #endregion

        #region Planting

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Wait until all player plant all the seed from seed bag.</b>");
        
        meta_Planting.Invoke();
        SetupPlanting();

        //Wait until plant all the seed in the bag.
        yield return new WaitUntil(() => nextStepReady);

        #endregion

        #region Watering

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Wait until all of the crop is fully grow.</b>");
        
        meta_Watering.Invoke();
        SetupWatering();

        //Wait until plant is fully growing after waterd.
        yield return new WaitUntil(() => nextStepReady);

        #endregion

        #region Harvesting

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Wait until all of the crop is being harvested.</b>");
        
        meta_Harvesting.Invoke();
        SetupHarvesting();

        //Wait until player harvested all of the crop.
        yield return new WaitUntil(() => nextStepReady);

        #endregion

        #region Selling

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Wait until all of the crop got selled.</b>");
        
        meta_Selling.Invoke();
        SetupSelling();

        //Wait until all of the crop are selled.
        yield return new WaitUntil(() => nextStepReady);

        #endregion

        #region UseBat

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Wait until someone got hit by a bat.</b>");
        
        meta_UseBat.Invoke();
        SetupUseBat();

        //Wait until someone got hit by a bat.
        yield return new WaitUntil(() => nextStepReady);

        #endregion

        #region KarmaIntroduction

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Introduce karma system with detail for 1min.</b>");
        
        meta_KarmaIntro_PartOne.Invoke();

        //Introduce player to karma system and waited for 1 min.
        yield return new WaitForSeconds(30f);

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Introduce karma system with detail for 1min.</b>");
        
        meta_KarmaIntro_PartTwo.Invoke();

        //Introduce player to karma system and waited for 1 min.
        yield return new WaitForSeconds(20f);

        #endregion

        #region CompletedTutorial

        nextStepReady = false;

        if(isDebugging)
            Debug.Log($"<b><color=white>MetaOnboarding</color>: Completed the tutorial.</b>");
        
        meta_CompletedTutorial.Invoke();

        yield return new WaitForSeconds(15f);

        #endregion

        GM.instance.LoadMenu();
    }

    [Button("Continue To Next Step")]
    public void DoneWithThisStep()
    {
        nextStepReady = true;
    }

    private void AddPlayerControllers()
    {
        foreach (PlayerController player in PlayerManager.instance.allPlayerController)
        {
            allPlayers.Add(player);
        }
    }

    private void SetupUseMarket()
    {
        AddPlayerControllers();
        foreach (PlayerController player in allPlayers)
        {
            player.onPurchased += OnPurchaseFromMarket;
        }
    }

    private void OnPurchaseFromMarket(PlayerID playerID)
    {
        allPlayers.Remove(PlayerManager.instance.GetPlayerControllerByID(playerID));
        if(allPlayers.Count == 0)
        {
            foreach (PlayerController player in PlayerManager.instance.allPlayerController)
            {
                player.onPurchased -= OnPurchaseFromMarket;
            }

            nextStepReady = true;
        }
    }

    private void SetupPlanting()
    {
        AddPlayerControllers();
        foreach (PlayerController player in allPlayers)
        {
            player.onPlanted += OnPlantingOnField;
        }
    }

    private void OnPlantingOnField(PlayerID playerID)
    {
        allPlayers.Remove(PlayerManager.instance.GetPlayerControllerByID(playerID));
        if(allPlayers.Count == 0)
        {
            foreach (PlayerController player in PlayerManager.instance.allPlayerController)
            {
                player.onPlanted -= OnPlantingOnField;
            }

            nextStepReady = true;
        }
    }

    private void SetupWatering()
    {
        AddPlayerControllers();
        foreach (PlayerController player in allPlayers)
        {
            player.onWatered += OnCropIsFullyGrow;
        }

        PlantableTile.onFullyGrow += OnCropIsFullyGrow;
    }

    private void OnCropIsFullyGrow()
    {
        isHarvestable = true;
        CheckFullyGrow();
    }

    private void OnCropIsFullyGrow(PlayerID playerID)
    {
        allPlayers.Remove(PlayerManager.instance.GetPlayerControllerByID(playerID));
        CheckFullyGrow();
    }

    private void CheckFullyGrow()
    {
        if (allPlayers.Count == 0 && isHarvestable)
        {
            foreach (PlayerController player in PlayerManager.instance.allPlayerController)
            {
                player.onWatered -= OnCropIsFullyGrow;
            }

            PlantableTile.onFullyGrow -= OnCropIsFullyGrow;
            nextStepReady = true;
        }
    }

    private void SetupHarvesting()
    {
        AddPlayerControllers();
        PlantableTile.onHarvested += OnHarvested;
    }

    private void OnHarvested(PlayerID playerID)
    {
        allPlayers.Remove(PlayerManager.instance.GetPlayerControllerByID(playerID));
        if(allPlayers.Count == 0)
        {
            PlantableTile.onHarvested -= OnHarvested;

            nextStepReady = true;
        }
    }

    private void SetupSelling()
    {
        AddPlayerControllers();
        MarketController.onSellingCrop += OnSellingCrop;
    }

    private void OnSellingCrop(PlayerID playerID)
    {
        allPlayers.Remove(PlayerManager.instance.GetPlayerControllerByID(playerID));
        if(allPlayers.Count == 0)
        {
            MarketController.onSellingCrop -= OnSellingCrop;

            nextStepReady = true;
        }
    }

    private void SetupUseBat()
    {
        AddPlayerControllers();
        foreach (PlayerController player in allPlayers)
        {
            player.onHittedOther += OnHittedOther;
        }
    }

    private void OnHittedOther(PlayerID playerID)
    {
        allPlayers.Remove(PlayerManager.instance.GetPlayerControllerByID(playerID));
        if(allPlayers.Count == 0)
        {
            foreach (PlayerController player in PlayerManager.instance.allPlayerController)
            {
                player.onHittedOther -= OnHittedOther;
            }

            nextStepReady = true;
        }
    }

}