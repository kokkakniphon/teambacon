﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using Luminosity.IO;

public class GM : MonoBehaviour
{
    public static GM instance;

    [SerializeField, BoxGroup("General Settings")]
    private CropDB _cropDB;
    public CropDB cropDB { get => _cropDB; private set { _cropDB = value; }}

    [SerializeField, BoxGroup("General Settings")]
    private ItemDB _itemDB;
    public ItemDB itemDB { get => _itemDB; private set { _itemDB = value; }}

    [SerializeField, BoxGroup("General Settings")]
    private GameObject loadingScreen;

    [SerializeField, BoxGroup("General Settings")]
    private AudioMixer mainMixer;

    private int _currentSchemeIndex = 2;
    public int currentSchemeIndex { get => _currentSchemeIndex; set{ _currentSchemeIndex = value % 4; OnChangeControlScheme();} }

    [SerializeField, BoxGroup("Control Schemes")]
    private InputManager zeroKeyboard;
    [SerializeField, BoxGroup("Control Schemes")]
    private InputManager oneKeyboard;
    [SerializeField, BoxGroup("Control Schemes")]
    private InputManager twoKeyboard;
    [SerializeField, BoxGroup("Control Schemes")]
    private InputManager threeKeyboard;

    public delegate void noArgs();
    public static noArgs onFinishLoading;

    public bool isFirstTimeAtMenu = true;

    public bool isFistTimeRunningGame = false;

    private void Awake() 
    {
        DontDestroyOnLoad(this.gameObject);

        if(instance != null)
            Destroy(this.gameObject);
        else
            instance = this;

        isFistTimeRunningGame = (PlayerPrefs.GetInt("FirstTimePlayedGame") == 0);

        if(isFistTimeRunningGame == true)
        {
            PlayerPrefs.SetInt("FirstTimePlayedGame", 1);
        }
    }

    [Button]
    public void ResetFirstTimePlayedGame()
    {
        PlayerPrefs.SetInt("FirstTimePlayedGame", 0);
    }

    public void LoadMenu()
    {
        FakeLodeToScene("Menu");
    }

    public void LoadLeaderBoard()
    {
        StartCoroutine(StartLoadLeaderBoard());
    }

    IEnumerator StartLoadLeaderBoard()
    {
        FakeLodeToScene("Menu");
        yield return new WaitForSeconds(3f);
        Mainmenu.instance.OnLeaderBoardStart();
    }

    public void LoadGame(string value_str)
    {
        FakeLodeToScene(value_str, 2.5f);
    }

    public IEnumerator FadingAudio(AudioMixer mixer,float targetValue, float fadingDuration, int stepAmount)
    {
        float currentValue;
        mixer.GetFloat("MasterVolume", out currentValue);

        float lerpingValue = (targetValue - currentValue)/stepAmount;
        for (int i = 0; i < stepAmount; i++)
        {
            currentValue += lerpingValue;
            mixer.SetFloat("MasterVolume", currentValue);
            yield return new WaitForSeconds(fadingDuration/stepAmount);
        }
        mixer.SetFloat("MasterVolume", targetValue);
    }

    public void FakeLodeToScene()
    {
        StartCoroutine(fakeLoadingScreen());
    }
    public void FakeLodeToScene(string targetScene)
    {
        StartCoroutine(fakeLoadingScreen(targetScene,0));
    }
    public void FakeLodeToScene(string targetScene, float loadDealy)
    {
        StartCoroutine(fakeLoadingScreen(targetScene,loadDealy));
    }

    IEnumerator fakeLoadingScreen()
    {
        float currentLevel ;
        mainMixer.GetFloat("MasterVolume", out currentLevel);
        loadingScreen.SetActive(true);
        // Start loading
        StartCoroutine(FadingAudio(mainMixer, -80f, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // Completely loaded
        StartCoroutine(FadingAudio(mainMixer, currentLevel, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // End loading
        loadingScreen.SetActive(false);
        if(onFinishLoading != null)
            onFinishLoading.Invoke();
    }

    IEnumerator fakeLoadingScreen(string targetScene, float loadDealy)
    {
        float currentLevel ;
        mainMixer.GetFloat("MasterVolume", out currentLevel);
        yield return new WaitForSeconds(loadDealy);
        loadingScreen.SetActive(true);
        // Start loading
        StartCoroutine(FadingAudio(mainMixer, -80f, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // Completely loaded
        SceneManager.LoadScene(targetScene);
        StartCoroutine(FadingAudio(mainMixer, currentLevel, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // End loading
        loadingScreen.SetActive(false);
        if(onFinishLoading != null)
            onFinishLoading.Invoke();
    }

    private void OnChangeControlScheme()
    {
        InputManager.ClearInputManager();
        
        switch (TypeControlSchemeManager.GetControlSchemeFromIndex(currentSchemeIndex))
        {
            case TypeControlScheme.ZeroKeyboard:
                if(zeroKeyboard != null) zeroKeyboard.gameObject.SetActive(true);
                if(oneKeyboard != null) oneKeyboard.gameObject.SetActive(false);
                if(twoKeyboard != null) twoKeyboard.gameObject.SetActive(false);
                if(threeKeyboard != null) threeKeyboard.gameObject.SetActive(false);

                zeroKeyboard.SetupInputManager();

                break;
            case TypeControlScheme.OneKeyboard:
                if(zeroKeyboard != null) zeroKeyboard.gameObject.SetActive(false);
                if(oneKeyboard != null) oneKeyboard.gameObject.SetActive(true);
                if(twoKeyboard != null) twoKeyboard.gameObject.SetActive(false);
                if(threeKeyboard != null) threeKeyboard.gameObject.SetActive(false);

                oneKeyboard.SetupInputManager();

                break;
            case TypeControlScheme.TwoKeyboard:
                if(zeroKeyboard != null) zeroKeyboard.gameObject.SetActive(false);
                if(oneKeyboard != null) oneKeyboard.gameObject.SetActive(false);
                if(twoKeyboard != null) twoKeyboard.gameObject.SetActive(true);
                if(threeKeyboard != null) threeKeyboard.gameObject.SetActive(false);

                twoKeyboard.SetupInputManager();

                break;
            case TypeControlScheme.ThreeKeyboard:
                if(zeroKeyboard != null) zeroKeyboard.gameObject.SetActive(false);
                if(oneKeyboard != null) oneKeyboard.gameObject.SetActive(false);
                if(twoKeyboard != null) twoKeyboard.gameObject.SetActive(false);
                if(threeKeyboard != null) threeKeyboard.gameObject.SetActive(true);

                threeKeyboard.SetupInputManager();

                break;
        }
    }
}
